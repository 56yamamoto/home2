export PATH=/opt/homebrew/bin:$HOME/bin:$PATH

export EDITOR=/usr/bin/vi
export VISUAL=/usr/bin/vi
# export j64x=j64
# export CC=clang-13
export USE_OPENMP=1
export USE_OPENCL=1
export USE_MUTEX=1
# export JAVX2=1
export PRINT_THREADINFO=0
export USE_PRIMITIVETHREADS=1
export USE_BOXEDSPARSE=1
export JOPTHOME="/opt/j"
export JUSERPATH602="$HOME/share/jusr/j602"
export JUSERPATH701="$HOME/share/jusr/j701"
export JUSERPATH801="$HOME/share/jusr/j801"
export JUSERPATH802="$HOME/share/jusr/j802"
export JUSERPATH803="$HOME/share/jusr/j803"
export JUSERPATH804="$HOME/share/jusr/j804"
export JUSERPATH805="$HOME/share/jusr/j805"
export JUSERPATH806="$HOME/share/jusr/j806"
export JUSERPATH807="$HOME/share/jusr/j807"
export JUSERPATH901="$HOME/share/jusr/j901"
export JUSERPATH902="$HOME/share/jusr/j902"
export JUSERPATH903="$HOME/share/jusr/j903"
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export ELECTRON_ENABLE_LOGGING=true
export HDBDOTNETCORE=/Applications/sap/hdbclient/dotnetcore/v2.1
export ANDROID_NDK_ROOT=$HOME/android/android-ndk-r23
# export NDK_TOOLCHAIN_VERSION=4.9
export NDK_TOOLCHAIN_VERSION=clang
export ANDROID_NDK_HOST=linux-x86_64
export ANDROID_NDK_PLATFORM=android-14
export ANDROID_TARGET_ARCH=armeabi
export ANDROID_HOME=$HOME/android/android-sdk-linux
if [ "x86_64" = `uname -m` ]; then
export JAVA_HOME=/usr/lib/jvm/java
export LIBJVM_PATH=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/server
export JAVALD_PATH=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64
else
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-i386/jre
export LIBJVM_PATH=/usr/lib/jvm/java-8-openjdk-i386/jre/lib/i386/server
export JAVALD_PATH=/usr/lib/jvm/java-8-openjdk-i386/jre/lib/i386
fi
export MAKEFLAGS=-j7
export WINEDEBUG=-all
export JMAXTHREADS=62
export PYTHONPATH=$HOME/python_module

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups
# ... and ignore same sucessive entries.
export HISTCONTROL=ignoreboth

export MYSQL_USER=root
export MYSQL_PASSWORD=catking
export MYSQL_HOST=192.168.1.203
export USE_BOXEDSPARSE=1
export QT_SELECT=5

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
#alias l='ls -CF'
alias vg='valgrind --tool=memcheck --leak-check=full -v --show-leak-kinds=all '
alias jsb='js-beautify -r -j -a -s1 -n -f'
alias cdacc='cd $HOME/project/account'
alias cdadd7='cd $HOME/jal/addons7'
alias cdadd='cd $HOME/jal/addons'
alias cdb6='cd $HOME/jal/base'
alias cdb7='cd $HOME/jal/base7'
alias cdb8='cd $HOME/jal/base8'
alias cdb9='cd $HOME/jal/base9'
alias cdjc='cd $HOME/share/jusr/j903/user/config-linux'
alias cdjc6='cd $HOME/share/jusr/j602/user/config-linux'
alias cdjp='cd $HOME/project/jproject/trunk'
alias cdjs='cd $HOME/jsource/jgplsrc'
alias cdjsc='cd $HOME/dev/jsource'
alias cdmai='cd $HOME/dev/MAIcore'
alias cdmaia='cd $HOME/dev/MAIcore/algo'
alias cdmaie='cd $HOME/dev/MAIengine'
alias cdmait='cd $HOME/dev/MAIengine/jlibrary/bin'
alias cdmaig='cd $HOME/dev/MAIcore/gui'
alias cdlbfgs='cd $HOME/dev/lbfgs'
alias cdjd='cd $HOME/dev/jd-cdsrc'
alias cdjdo='cd $HOME/dev/jd'
alias cdjn='cd $HOME/dev/jnet'
alias cdjo='cd $HOME/dev/jdodbc'
alias cdsq='cd $HOME/dev/sqlite'
alias cdjdist='cd $HOME/dev/jdist'
alias cddeb='cd $HOME/dev/debpackage'
alias cdte='cd $HOME/.wine/drive_c/Program\ Files\ \(x86\)/Microsoft\ Data\ Access\ SDK\ 2.8/Tools/amd64'
alias cdjsoft='cd $HOME/share/jsoftware'
alias cdpub7='cd $HOME/jal/public7'
alias cdpub='cd $HOME/jal/public'
alias cdtmp='cd $HOME/.wine/drive_c/windows/temp'
alias cdwi='cd $HOME/share/willis'
alias cdqt='cd $HOME/dev/apps/ide/jqt'
alias cdqtw='cd $HOME/src/qt-everywhere-src-5.14.0/qtbase/src/widgets/widgets'
alias cdjca='cd $HOME/dev/apps/ide/jandroid'
alias cdje='cd $HOME/dev/apps/ide/jedi'
alias cdlu='cd $HOME/luenthai'
alias cdlu0='cd $HOME/luenthai/2018-12/nodejs'
alias cdlu1='cd $HOME/luenthai/2019-01/imappssp/sp'
alias cdlu2='cd $HOME/luenthai/2019-03/select'
alias cdlu3='cd $HOME/luenthai/2019-05/jdselect'
alias cdjar='cd $HOME/dev/apps/ide/jjar'
alias cdj6='cd $HOME/share/jsoftware/j602/bin'
alias sshraspi='TERM=screen-256color ssh pi@raspi'
alias cdbd='cd $HOME/dev/jsource/jlibrary/bin'
alias cdbd32='cd $HOME/dev/jsource/jlibrary/bin32'

alias rmi='rm -I'
alias cpi='cp -i'
alias winecleanmake='cd $HOME/src/wine64 && make clean && wine64make && cd $HOME/src/winewow64 && make clean && winewow64make'
alias winemake='cd $HOME/src/wine64 && wine64make && cd $HOME/src/winewow64 && winewow64make'
alias winewowmake='cd $HOME/src/winewow64 && winewow64make'
alias wine32xmake='cd $HOME/src/wine32 && wine32make'
alias findn="find . -not -path '*/\.*'"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Load version control information
autoload -Uz vcs_info
precmd() { vcs_info }

# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats '(%b)'
 
# Set up the prompt (with git branch name)
setopt PROMPT_SUBST
PROMPT='%n:${PWD/#$HOME/~}${vcs_info_msg_0_}%% '
