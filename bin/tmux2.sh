#!/bin/sh

tmux -2 new-session -d -n ncmpc -s tmux ncmpc \
\; new-window -n vifm -t tmux:1 "vifm $HOME" \
\; new-window -n mutt -t tmux:2 "mutt -f imaps://bbill.lam@imap.gmail.com/INBOX" \
\; new-window -n w3m -t tmux:3 "/usr/bin/www-browser ~" \
\; new-window -n bash -t tmux:4 "/bin/bash" \
\; new-window -n bash -t tmux:5 "/bin/bash" \
\; new-window -n bash -t tmux:6 "/bin/bash" \
\; new-window -n bash -t tmux:7 "$HOME/bin/pullaccount ; /bin/bash" \
\; new-window -n bash -t tmux:8 "/bin/bash" \
\; new-window -n bash -t tmux:9 "/bin/bash" \
\; select-window -t tmux:2 \
\; attach
#\; run-shell fbdefkb
