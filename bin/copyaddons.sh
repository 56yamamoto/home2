#!/bin/bash
umask 022
JVERS1=${JVERS1:-9.03}
JVERS2=${JVERS1//\.}
if [ -z "${JVERS2##*80*}" ]; then
JBASE=base8
else
JBASE=base9
fi
echo "jversion $JVERS1 $JVERS2 $JBASE"

if [ "Linux" = "$(uname)" ]; then
SHR=/usr/share
sudo cp -r $HOME/jal/$JBASE/release/install/system $SHR/j/$JVERS1/.
sudo chown -R root:root $SHR/j/$JVERS1/system
sudo cp -r $HOME/jal/addons $SHR/j/$JVERS1/.
sudo find $SHR/j/$JVERS1/addons -type d -name '.*'|xargs sudo rm -rf
sudo find $SHR/j/$JVERS1/addons -type f -name '.*'|xargs sudo rm -f
sudo find $SHR/j/$JVERS1/addons -type d -name 'save'|xargs sudo rm -rf
sudo find $SHR/j/$JVERS1/addons -type d -name 'source'|xargs sudo rm -rf
sudo find $SHR/j/$JVERS1/addons -type d -iname 'release'|xargs sudo rm -rf
sudo rm -f $SHR/j/$JVERS1/addons/data/jd/*.csv
sudo rm -rf $SHR/j/$JVERS1/addons/data/jd/.git
sudo rm -rf $SHR/j/$JVERS1/addons/data/jd/cdsrc
sudo rm -rf $SHR/j/$JVERS1/addons/data/jd/bugs
sudo rm -rf $SHR/j/$JVERS1/addons/data/jd/xtra
# sudo cp $HOME/project/jproject/trunk/jlib/jodbc.ijs $SHR/j/$JVERS1/addons/data/odbc/odbc.ijs

sudo cp $HOME/dev/jd-cdsrc/libjd.so $SHR/j/$JVERS1/addons/data/jd/cd/.
sudo cp $HOME/dev/jd-cdsrc/libjd.dylib $SHR/j/$JVERS1/addons/data/jd/cd/. || true
sudo cp $HOME/dev/jd-cdsrc/jd.dll   $SHR/j/$JVERS1/addons/data/jd/cd/.
# uncomment following 2 lines when jd repo ready
# sudo cp $HOME/jal/addons/data/jd/cd/libjd.so $SHR/j/$JVERS1/addons/data/jd/cd/.
# sudo cp $HOME/jal/addons/data/jd/cd/jd.dll $SHR/j/$JVERS1/addons/data/jd/cd/.
sudo cp $HOME/luenthai/2019-03/cjoin/libjoin.so $SHR/j/$JVERS1/addons/data/jd/cd/.
sudo cp $HOME/luenthai/2019-03/cjoin/libjoin.dylib $SHR/j/$JVERS1/addons/data/jd/cd/. || true
sudo cp $HOME/luenthai/2019-03/cjoin/join.dll $SHR/j/$JVERS1/addons/data/jd/cd/.
sudo cp $HOME/luenthai/2019-05/jdselect/release/*.ijs $SHR/j/$JVERS1/addons/data/jd/api/.
sudo cp $HOME/luenthai/2020-02/cubedemo/jdcube.ijs $SHR/j/$JVERS1/addons/data/jd/api/.
sudo cp $HOME/luenthai/2020-05/blobvarbyte/blobvarbyte.ijs $SHR/j/$JVERS1/addons/data/jd/api/.
sudo find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sudo sed -i "s/^'Jd requires J807/NB. 'Jd requires J807/g"
sudo find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sudo sed -i "s/^forcecopy_z_=: 15!:15 /forcecopy_z_=: '' 1 : 'try. 15!:15 m catch. a: { ] return. end. 15!:15' /g"
# sudo find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sudo sed -i "s/ opx=\. / opx=. ('select'-:OP){::opx;'x' [ opx=. /"
sudo find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sudo sed -i "s/^ROOPS=: '/ROOPS=: 'exec';'select';'xcubeget';'xcubeshow';'xcubeshowvar';'xcubenextvar';'xcubeerasevar';'xcubeshowsummary';'/"
sudo find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sudo sed -i "s/^api\/client\.ijs$/api\/client\.ijs\napi\/api_select.ijs\napi\/luenthaisql.ijs\napi\/jdcube.ijs\napi\/blobvarbyte.ijs/"
# sudo find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sudo sed -i "s/if\. JDMT=MTRO_jmf_ do\. mtmfixcount c end\./NB. if. JDMT=MTRO_jmf_ do. mtmfixcount c end./"
sudo find $SHR/j/$JVERS1/addons/data/jd/doc -name '*.htm'|xargs sudo sed -i "s/code>/pre>/g"
sudo find $SHR/j/$JVERS1/addons/data/jd/doc -name 'jd.css'|xargs sudo sed -i "s/^code /pre /"

sudo rm -rf $SHR/j/$JVERS1/addons/production/jd
sudo mkdir -p $SHR/j/$JVERS1/addons/production/
sudo cp -r  $SHR/j/$JVERS1/addons/data/jd $SHR/j/$JVERS1/addons/production/.

sudo chown -R root:root $SHR/j/$JVERS1/addons
sudo find $SHR/j/$JVERS1/addons -type f -exec chmod 644 {} \+
sudo find $SHR/j/$JVERS1/addons -type d -exec chmod 755 {} \+

else
# macos

if [ "arm64" = "$(uname -m)" ]; then
SHR=/opt/homebrew/share
else
SHR=/usr/local/share
fi

cp -r $HOME/jal/$JBASE/release/install/system $SHR/j/$JVERS1/.
cp -r $HOME/jal/addons $SHR/j/$JVERS1/.
find $SHR/j/$JVERS1/addons -type d -name '.*'|xargs rm -rf
find $SHR/j/$JVERS1/addons -type f -name '.*'|xargs rm -f
find $SHR/j/$JVERS1/addons -type d -name 'save'|xargs rm -rf
find $SHR/j/$JVERS1/addons -type d -name 'source'|xargs rm -rf
find $SHR/j/$JVERS1/addons -type d -iname 'release'|xargs rm -rf
rm -f $SHR/j/$JVERS1/addons/data/jd/*.csv
rm -rf $SHR/j/$JVERS1/addons/data/jd/.git
rm -rf $SHR/j/$JVERS1/addons/data/jd/cdsrc
rm -rf $SHR/j/$JVERS1/addons/data/jd/bugs
rm -rf $SHR/j/$JVERS1/addons/data/jd/xtra
# cp $HOME/project/jproject/trunk/jlib/jodbc.ijs $SHR/j/$JVERS1/addons/data/odbc/odbc.ijs

cp $HOME/dev/jd-cdsrc/libjd.so $SHR/j/$JVERS1/addons/data/jd/cd/.
cp $HOME/dev/jd-cdsrc/libjd.dylib $SHR/j/$JVERS1/addons/data/jd/cd/. || true
cp $HOME/dev/jd-cdsrc/jd.dll   $SHR/j/$JVERS1/addons/data/jd/cd/.
# uncomment following 2 lines when jd repo ready
# cp $HOME/jal/addons/data/jd/cd/libjd.so $SHR/j/$JVERS1/addons/data/jd/cd/.
# cp $HOME/jal/addons/data/jd/cd/jd.dll $SHR/j/$JVERS1/addons/data/jd/cd/.
cp $HOME/luenthai/2019-03/cjoin/libjoin.so $SHR/j/$JVERS1/addons/data/jd/cd/.
cp $HOME/luenthai/2019-03/cjoin/libjoin.dll $SHR/j/$JVERS1/addons/data/jd/cd/.
cp $HOME/luenthai/2019-03/cjoin/join.dylib $SHR/j/$JVERS1/addons/data/jd/cd/. || true
cp $HOME/luenthai/2019-05/jdselect/release/*.ijs $SHR/j/$JVERS1/addons/data/jd/api/.
cp $HOME/luenthai/2020-02/cubedemo/jdcube.ijs $SHR/j/$JVERS1/addons/data/jd/api/.
cp $HOME/luenthai/2020-05/blobvarbyte/blobvarbyte.ijs $SHR/j/$JVERS1/addons/data/jd/api/.
find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sed -i "s/^'Jd requires J807/NB. 'Jd requires J807/g"
find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sed -i "s/^forcecopy_z_=: 15!:15 /forcecopy_z_=: '' 1 : 'try. 15!:15 m catch. a: { ] return. end. 15!:15' /g"
# find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sed -i "s/ opx=\. / opx=. ('select'-:OP){::opx;'x' [ opx=. /"
find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sed -i "s/^ROOPS=: '/ROOPS=: 'exec';'select';'xcubeget';'xcubeshow';'xcubeshowvar';'xcubenextvar';'xcubeerasevar';'xcubeshowsummary';'/"
find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sed -i "s/^api\/client\.ijs$/api\/client\.ijs\napi\/api_select.ijs\napi\/luenthaisql.ijs\napi\/jdcube.ijs\napi\/blobvarbyte.ijs/"
# find $SHR/j/$JVERS1/addons/data/jd -name '*.ijs'|xargs sed -i "s/if\. JDMT=MTRO_jmf_ do\. mtmfixcount c end\./NB. if. JDMT=MTRO_jmf_ do. mtmfixcount c end./"
find $SHR/j/$JVERS1/addons/data/jd/doc -name '*.htm'|xargs sed -i "s/code>/pre>/g"
find $SHR/j/$JVERS1/addons/data/jd/doc -name 'jd.css'|xargs sed -i "s/^code /pre /"

rm -rf $SHR/j/$JVERS1/addons/production/jd
mkdir -p $SHR/j/$JVERS1/addons/production/
cp -r  $SHR/j/$JVERS1/addons/data/jd $SHR/j/$JVERS1/addons/production/.

find $SHR/j/$JVERS1/addons -type f -exec chmod 644 {} \+
find $SHR/j/$JVERS1/addons -type d -exec chmod 755 {} \+

fi

rm -rf $HOME/share/ins.d/j$JVERS2-win/system
cp -r $SHR/j/$JVERS1/system $HOME/share/ins.d/j$JVERS2-win/
mkdir -p $HOME/share/ins.d/j$JVERS2-win/bin
mkdir -p $HOME/share/ins.d/j$JVERS2-win/bin32
cp $HOME/jal/$JBASE/config/profile.ijs $HOME/share/ins.d/j$JVERS2-win/bin/.
cp $HOME/jal/$JBASE/config/profile.ijs $HOME/share/ins.d/j$JVERS2-win/bin32/.
rm -rf $HOME/share/ins.d/j$JVERS2-win/addons
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/arc
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/convert
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/data
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/general
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/graphics
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/ide
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/math
mkdir -p $HOME/share/ins.d/j$JVERS2-win/addons/tables
cp -r $SHR/j/$JVERS1/addons/arc/zlib $HOME/share/ins.d/j$JVERS2-win/addons/arc
cp -r $SHR/j/$JVERS1/addons/convert/misc $HOME/share/ins.d/j$JVERS2-win/addons/convert
cp -r $SHR/j/$JVERS1/addons/data/ddsqlite $HOME/share/ins.d/j$JVERS2-win/addons/data
cp -r $SHR/j/$JVERS1/addons/data/rdd $HOME/share/ins.d/j$JVERS2-win/addons/data
cp -r $SHR/j/$JVERS1/addons/data/jd $HOME/share/ins.d/j$JVERS2-win/addons/data
cp -r $SHR/j/$JVERS1/addons/data/jfiles $HOME/share/ins.d/j$JVERS2-win/addons/data
cp -r $SHR/j/$JVERS1/addons/data/jmf $HOME/share/ins.d/j$JVERS2-win/addons/data
cp -r $SHR/j/$JVERS1/addons/data/odbc $HOME/share/ins.d/j$JVERS2-win/addons/data
cp -r $SHR/j/$JVERS1/addons/general/misc $HOME/share/ins.d/j$JVERS2-win/addons/general
cp -r $SHR/j/$JVERS1/addons/graphics/afm $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/graphics/bmp $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/graphics/color $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/graphics/gl2 $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/graphics/jpeg $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/graphics/plot $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/graphics/png $HOME/share/ins.d/j$JVERS2-win/addons/graphics
cp -r $SHR/j/$JVERS1/addons/ide/jhs $HOME/share/ins.d/j$JVERS2-win/addons/ide
cp -r $SHR/j/$JVERS1/addons/ide/jnet $HOME/share/ins.d/j$JVERS2-win/addons/ide
rm -rf $HOME/share/ins.d/j$JVERS2-win/addons/ide/jnet/bin
rm -rf $HOME/share/ins.d/j$JVERS2-win/addons/ide/jnet/data
rm -rf $HOME/share/ins.d/j$JVERS2-win/addons/ide/jnet/demo
rm -rf $HOME/share/ins.d/j$JVERS2-win/addons/ide/jnet/js
rm -rf $HOME/share/ins.d/j$JVERS2-win/addons/ide/j6wd
cp -r $SHR/j/$JVERS1/addons/math/misc $HOME/share/ins.d/j$JVERS2-win/addons/math
cp -r $SHR/j/$JVERS1/addons/tables/tara $HOME/share/ins.d/j$JVERS2-win/addons/tables
cp -r $SHR/j/$JVERS1/addons/tables/taraxml $HOME/share/ins.d/j$JVERS2-win/addons/tables
cp -r $SHR/j/$JVERS1/addons/tables/wdooo $HOME/share/ins.d/j$JVERS2-win/addons/tables
cp $HOME/project/jproject/trunk/jlib/jodbc.ijs $HOME/share/ins.d/j$JVERS2-win/addons/data/odbc/odbc.ijs
cp $HOME/project/jproject/trunk/resource/profilex.ijs $HOME/share/ins.d/j$JVERS2-win/bin/.
cp $HOME/project/jproject/trunk/resource/profilex.ijs $HOME/share/ins.d/j$JVERS2-win/bin32/.
# find $HOME/share/ins.d/j$JVERS2-win -name 'manifest.ijs' -delete
find $HOME/share/ins.d/j$JVERS2-win -name 'history.txt' -delete
find $HOME/share/ins.d/j$JVERS2-win -name '*.jproj' -delete
find $HOME/share/ins.d/j$JVERS2-win -name '*.so' -delete
find $HOME/share/ins.d/j$JVERS2-win -name '*.dylib' -delete
find $HOME/share/ins.d/j$JVERS2-win -name '*.ijt' -delete
find $HOME/share/ins.d/j$JVERS2-win -name 'tara-504.ijs' -delete
find $HOME/share/ins.d/j$JVERS2-win -type d -name 'test' -print0|xargs -0 rm -rf
find $HOME/share/ins.d/j$JVERS2-win -type d -name 'demo' -print0|xargs -0 rm -rf

# jdkey
# cp $HOME/conf.backup/jdkey.txt $HOME/share/ins.d/j$JVERS2-win/addons/data/jd/config/.

if [ "Linux" = "$(uname)" ]; then
sudo rm -rf $SHR/j/6.02/addons
sudo mkdir -p $SHR/j/6.02/addons
sudo cp -r $SHR/j/$JVERS1/addons $SHR/j/6.02/.
sudo find $SHR/j/6.02/addons -type f -exec chmod 644 {} \+
sudo find $SHR/j/6.02/addons -type d -exec chmod 755 {} \+
else
rm -rf $SHR/j/6.02/addons
mkdir -p $SHR/j/6.02/addons
cp -r $SHR/j/$JVERS1/addons $SHR/j/6.02/.
find $SHR/j/6.02/addons -type f -exec chmod 644 {} \+
find $SHR/j/6.02/addons -type d -exec chmod 755 {} \+
fi

# sudo mkdir -p $SHR/j/6.02/addons/data
# sudo mkdir -p $SHR/j/6.02/addons/ide
# sudo cp -r $HOME/jal/addons/data/ddsqlite $SHR/j/6.02/addons/data
# sudo cp -r $HOME/jal/addons/data/rdd $SHR/j/6.02/addons/data
# sudo cp -r $HOME/jal/addons/data/jfiles $SHR/j/6.02/addons/data
# sudo cp -r $HOME/jal/addons/data/jmf $SHR/j/6.02/addons/data
# sudo cp -r $HOME/jal/addons/data/odbc $SHR/j/6.02/addons/data
# sudo cp -r $HOME/jal/addons/ide/jhs $SHR/j/6.02/addons/ide
# sudo chown -R root:root $SHR/j/6.02/addons
# sudo find $SHR/j/6.02/addons -type f -exec chmod 644 {} \+
# sudo find $SHR/j/6.02/addons -type d -exec chmod 755 {} \+

# cd $HOME/share/ins.d/j$JVERS2-win/bin && wine64 ./jconsole.exe -js "exit''[refreshaddins_jpacman_''[load'pacman'"

umask 027
