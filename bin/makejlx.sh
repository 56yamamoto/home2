#!/bin/sh

cd $HOME/dev/jsource/make2 && \
./clean.sh && \
jplatform=linux j64x=j64 ./build_libj.sh && \
./clean.sh && \
jplatform=linux j64x=j64avx ./build_libj.sh && \
./clean.sh && \
jplatform=linux j64x=j64avx2 ./build_jconsole.sh && \
jplatform=linux j64x=j64avx2 ./build_libj.sh && \
jplatform=linux j64x=j64avx2 ./build_tsdll.sh && \
jplatform=linux j64x=j64avx2 ./build_jnative.sh && \
./clean.sh && \
jplatform=linux j64x=j32 ./build_jconsole.sh && \
jplatform=linux j64x=j32 ./build_libj.sh && \
jplatform=linux j64x=j32 ./build_tsdll.sh && \
jplatform=linux j64x=j32 ./build_jnative.sh && \
./cpbin.sh

# cpylib.sh
echo cpylib.sh
