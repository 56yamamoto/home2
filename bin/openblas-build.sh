#!/bin/sh

# PRESCOTT    Pentium 4 sse2
# SANDYBRIDGE AVX

# make -j4 OSNAME=WINNT CROSS=1 TARGET=SANDYBRIDGE BINARY=64 HOSTCC=gcc CC=x86_64-w64-mingw32-gcc FC=x86_64-w64-mingw32-gfortran RANLIB=x86_64-w64-mingw32-ranlib NO_LAPACKE=1 NO_AFFINITY=1 USE_THREAD=1 USE_OPENMP=1 NUM_THREADS=32 CFLAGS='-static-libgcc -static-libstdc++ -static' FFLAGS='-static'
make -j4 OSNAME=WINNT CROSS=1 DYNAMIC_ARCH=1 DYNAMIC_LIST='SANDYBRIDGE' BINARY=64 HOSTCC=gcc CC=x86_64-w64-mingw32-gcc FC=x86_64-w64-mingw32-gfortran RANLIB=x86_64-w64-mingw32-ranlib NO_AFFINITY=1 USE_THREAD=1 USE_OPENMP=1 NUM_THREADS=32 CFLAGS='-static-libgcc -static-libstdc++ -static -Wl,--dynamicbase -Wl,--nxcompat -Wl,--high-entropy-va' FFLAGS='-static'

