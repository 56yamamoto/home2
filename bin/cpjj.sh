#!/bin/sh
# adb push ~/jal/addons/ide/ja/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/ide/ja/
# adb push ~/jal/addons/api/gles/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/api/gles/
# adb push ~/jal/addons/demos/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/demos/
# adb push ~/jal/addons/games/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/games/
# adb push ~/jal/addons/graphics/gl2/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/graphics/gl2/
# adb push ~/jal/addons/graphics/graph/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/graphics/graph/
# adb push ~/jal/addons/graphics/viewmat/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/graphics/viewmat/
# adb push ~/jal/addons/graphics/plot/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/graphics/plot/
# adb push ~/jal/addons/labs/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/labs/
adb push ~/jal/addons/api/opencl/ /sdcard/Android/data/com.jsoftware.j.android/files/addons/api/opencl/
adb shell rm -rf /sdcard/Android/data/com.jsoftware.j.android/files/addons/api/opencl/.git
# adb pull /sdcard/Android/data/com.jsoftware.j.android/files/addons/api/opencl/ ~/jal/addons/api/opencl/ 
