#!/bin/sh
if [ "`uname -m`" = "arm64" ]; then
WINEPREFIX="$HOME/.wine" wine64 $HOME/share/jsoftware/j903/bin/jconsole.exe -norl -lib j-nonavx.dll "$@"
else
WINEPREFIX="$HOME/.wine" wine64 $HOME/share/jsoftware/j903/bin/jconsole.exe -norl -lib javx2.dll "$@"
fi
