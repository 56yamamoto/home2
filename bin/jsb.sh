#!/bin/bash
# js-beautify -r -j -s 1 -n -f "$@"

DIRS="."
if [[ "$1" == --dir ]]; then
    shift 1
    DIRS="$1"
    shift 1
fi

find $DIRS -not -path '*/\.*' -not \( -path '*/.*/*' -prune \) -not \( -path '*/jquery/*' -prune \) -not \( -path '*/codemirror/*' -prune \) -not \( -path '*/d3/*' -prune \) -not \( -path '*/handsontable/*' -prune \) -not \( -path '*/libs/*' -prune \) -not \( -path '*/node_modules/*' -prune \) -not \( -path '*/.git/*' -prune \) -name '*.js' -exec js-beautify -r -j -a -s 1 -n -f {} \+
