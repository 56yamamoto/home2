#!/bin/sh

tmux -2 new-session -s tmux2 -n ncmpc \
\; new-window -n vifm -t tmux2:1 "vifm $HOME" \
\; new-window -n mutt -t tmux2:2 "mutt -f imaps://jbill.lam@imap.gmail.com/INBOX" \
\; new-window -n rss -t tmux2:3 "mutt -f imaps://rss.mutt@imap.aol.com/INBOX" \
\; new-window -n w3m -t tmux2:4 "/usr/bin/www-browser ~" \
\; new-window -n bash -t tmux2:5 "/bin/bash" \
\; new-window -n wyrd -t tmux2:6 "/bin/bash" \
\; new-window -n wyrd -t tmux2:7 "$HOME/bin/pullaccount" \
\; select-window -t tmux2:2 \
\; attach -t tmux2
#\; run-shell fbdefkb
