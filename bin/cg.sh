#!/bin/sh
mkdir -p $HOME/jal/addons/$1
cd $HOME/jal/addons
git submodule add git@github.com:jsoftware/$1_$2.git $1/$2
