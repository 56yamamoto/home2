#!/bin/sh

if [ "`uname`" = "Darwin" ]; then
#macos
cd $HOME/dev/jsource/make2 && \
./clean.sh && \
jplatform=darwin j64x=j64arm ./build_jconsole.sh && \
jplatform=darwin j64x=j64arm ./build_libj.sh && \
jplatform=darwin j64x=j64arm ./build_tsdll.sh && \
./clean.sh && \
jplatform=darwin j64x=j64 ./build_jconsole.sh && \
jplatform=darwin j64x=j64 ./build_libj.sh && \
jplatform=darwin j64x=j64 ./build_tsdll.sh && \
./cpbin.sh && \
 \
cd $HOME/dev/jsource/makemsvc/tsdll && \
clangcl make clean && clangcl make && \
clangcl32 make clean && clangcl32 make && \
 \
cd $HOME/dev/jsource/makemsvc/jconsole && \
clangcl make clean && clangcl make && \
clangcl32 make clean && clangcl32 make && \
 \
cd $HOME/dev/jsource/makemsvc/jdll && \
clangcl32 make clean && clangcl32 make USE_OPENMP=1 && \
clangcl make clean && clangcl make JAVX2=1 USE_OPENMP=1 && \
clangcl make clean && clangcl make JAVX2=0 JAVX=1 USE_OPENMP=1 && \
clangcl make clean && clangcl make JAVX2=0 JAVX=0 USE_OPENMP=1 && \
 \
cpyj.sh && cpybeta.sh \

else
#linux
cd $HOME/dev/jsource/make2 && \
./clean.sh && \
jplatform=linux j64x=j64 ./build_libj.sh && \
./clean.sh && \
jplatform=linux j64x=j64avx ./build_libj.sh && \
./clean.sh && \
jplatform=linux j64x=j64avx2 ./build_jconsole.sh && \
jplatform=linux j64x=j64avx2 ./build_libj.sh && \
jplatform=linux j64x=j64avx2 ./build_tsdll.sh && \
jplatform=linux j64x=j64avx2 ./build_jnative.sh && \
./clean.sh && \
jplatform=linux j64x=j32 ./build_jconsole.sh && \
jplatform=linux j64x=j32 ./build_libj.sh && \
jplatform=linux j64x=j32 ./build_tsdll.sh && \
jplatform=linux j64x=j32 ./build_jnative.sh && \
./cpbin.sh && \
 \
cd $HOME/dev/jsource/makemsvc/tsdll && \
clangcl make clean && clangcl make && \
clangcl32 make clean && clangcl32 make && \
 \
cd $HOME/dev/jsource/makemsvc/jconsole && \
clangcl make clean && clangcl make && \
clangcl32 make clean && clangcl32 make && \
 \
cd $HOME/dev/jsource/makemsvc/jdll && \
clangcl32 make clean && clangcl32 make USE_OPENMP=1 && \
clangcl make clean && clangcl make JAVX2=1 USE_OPENMP=1 && \
clangcl make clean && clangcl make JAVX2=0 JAVX=1 USE_OPENMP=1 && \
clangcl make clean && clangcl make JAVX2=0 JAVX=0 USE_OPENMP=1 && \
 \
cpyj.sh && cpybeta.sh \

fi
# cpylib.sh
echo cpylib.sh
