NB. build all addons jdb pubish
cocurrent 'base'

ignorehash=: 0

F1=: <;._2 (0 : 0)
# api/gtkinclude
# api/gles2
api/ncurses
arc/ziptrees
convert/jiconv
demos/isigraph
demos/opengl
demos/plot
demos/wd
# docs/gtk
# docs/help
docs/joxygen/source
docs/wikihtml
finance/actuarial
finance/interest
games/minesweeper
games/nurikabe
games/pousse
games/solitaire
general/dirtrees
general/dirutils
general/inifiles/source
general/jtags
general/sfl
general/userdict
graphics/gnuplot
graphics/jturtle
graphics/treemap
gui/monthview
ide/jhs
# labs/labs/run
math/deoptim/source
math/eigenpic
math/fftw
math/misc
math/mt
math/tabula
media/image3
media/platimg/test
net/clientserver
stats/base
stats/distribs/normal
stats/r/dcmd
stats/r/dcom
stats/r/rbase
stats/r/rserve
stats/rlibrary/splines
types/datetime/source
web/gethttp
)

f1=: 3 : 0
smoutput y
if. '#'={.y do.
  if. -.ignorehash do. i.0 0 return. end.
  y=. }.y
end.
load '~Addons/', y, '/build.ijs'
i.0 0
)

F2=: <;._2 (0 : 0)
)

f2=: 3 : 0
smoutput y
if. '#'={.y do.
  if. -.ignorehash do. i.0 0 return. end.
  y=. }.y
end.
load '~Addons/', y, '/save.ijs'
i.0 0
)

NB. will add to F3 convert/jgettext/source
F3=: <;._2 (0 : 0)
api/android
api/expat/source
api/jni/source
#convert/gtkdoctag/source
convert/json/source
data/ddmysql/source
data/ddsqlite/source
data/jdb/base/save
data/jfiles/source
# data/jmf/source
data/odbc/source
general/scriptdoc/source
graphics/bmp/source
graphics/graph/source
graphics/opengl/main
graphics/opengl/util
# graphics/print/print
graphics/viewmat/source
gui/droidwd/source
gui/gtkwd/source
gui/jgtkgrid/source
math/lapack
tables/csv/source
tables/dsv/source
tables/excel
# tables/tara/source
tables/taraxml/source
tables/wdooo/source
)

f3=: 3 : 0
smoutput y
if. '#'={.y do.
  if. -.ignorehash do. i.0 0 return. end.
  y=. }.y
end.
load '~Public/', y, '/build.ijs'
i.0 0
)

NB. data/jdb/base/save
F4=: <;._2 (0 : 0)
#convert/gtkdoctag/source
format/publish/source/base/save
graphics/afm/source/afm
graphics/gl2/source/save
graphics/grid/source/save
graphics/plot/source/base/save
gui/android/source/save
gui/gtk/source/save
gui/wdclass/source/save
ide/gtk/source/save
ide/qt/source/save
)

f4=: 3 : 0
smoutput y
if. '#'={.y do.
  if. -.ignorehash do. i.0 0 return. end.
  y=. }.y
end.
load '~Public/', y, '/save.ijs'
i.0 0
)


NB. data/jdb/base/save
F5=: <;._2 (0 : 0)
libglcmds.so
libglcmds.dll
libglcmds_32.so
libglcmds_32.dll
)

f5=: 3 : 0
smoutput y
if. '#'={.y do.
  if. -.ignorehash do. i.0 0 return. end.
  y=. }.y
end.
(<jpath '~Addons/graphics/gl2/',y) 1!:2~ 1!:1 <jpath '~Public/graphics/gl2/release/',y
(<jpath '~addons/graphics/gl2/',y) 1!:2~ 1!:1 <jpath '~Public/graphics/gl2/release/',y
i.0 0
)

load 'project'

NB. uncomment to retain comment for comparison with J6
NB. writesourcex_jp_ =: writesource_jp_

f1 &.> F1
f2 &.> F2
f3 &.> F3
f4 &.> F4
NB. f5 &.> F5
