# find . -name '*.o' -exec rm \"{}\" \;
find . -name "*~" -delete
find . -name "*.~c" -delete
find . -name "*.~h" -delete
find . -name "*.~rc" -delete
find . -name "*.aux" -delete
find . -name "*.bak" -delete
find . -name "*.dvi" -delete
find . -name "*.hlog" -delete
find . -name "*.log" -delete
find . -name "*.o" -delete
find . -name "*.obj" -delete
find . -name "*.pyc" -delete
find . -name "*.toc" -delete

