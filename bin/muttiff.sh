#!/bin/sh
tf=`mktemp`
# copy mutt's tempfile so it's not removed too early
# trap "rm -f $tf $tf.pdf" 1 2 3 13 15
cp "$1" "$tf"
tiff2pdf -z "$tf" > "$tf.pdf"
xpdf "$tf.pdf" && rm -f "$tf" "$tf.pdf"
