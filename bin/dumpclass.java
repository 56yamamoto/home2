/* 
┌────────────────────────────────────────────────┐
│ java.lang.reflect.Modifier                     │
├──────────────────────────┬──────────────┬──────┤
│  public static final int │ ABSTRACT     │ 1024 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ FINAL        │   16 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ INTERFACE    │  512 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ NATIVE       │  256 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ PRIVATE      │    2 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ PROTECTED    │    4 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ PUBLIC       │    1 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ STATIC       │    8 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ STRICT       │ 2048 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ SYNCHRONIZED │   32 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ TRANSIENT    │  128 │
├──────────────────────────┼──────────────┼──────┤
│  public static final int │ VOLATILE     │   64 │
├──────────────────────────┼──────────────┼──────┤
└──────────────────────────┴──────────────┴──────┘
*/

import java.lang.reflect.*;
import java.lang.StringBuilder;

public class dumpclass {

    private static  StringBuilder buffer;

    private static void print (String s) { buffer = buffer.append(s);}
    private static void println (String s) { print(s + "\n");}

    private static final boolean ignoredeprecated = false;
    public static void main(String[] args) {
        try {
   int length = args.length;
   if (length <= 0) {
   System.out.println("You need to enter one arguments.");
   System.exit(1);
   }
            Class clz = Class.forName(args[0]);

            if(ignoredeprecated && clz.isAnnotationPresent(java.lang.Deprecated.class)) System.exit(0);

            Field[] flds = clz.getDeclaredFields();
            buffer = new StringBuilder();

            int clsmodifiers = clz.getModifiers();
/*
            if (Modifier.isAbstract(clsmodifiers)) System.exit(0);
            if (Modifier.isFinal(clsmodifiers)) System.exit(0);
            if (Modifier.isInterface(clsmodifiers)) System.exit(0);
            if (Modifier.isNative(clsmodifiers)) System.exit(0);
*/
            println("\n=== " + args[0] + " ===");
            println(Modifier.toString( clsmodifiers));
            println("\n...Variables...");
            for ( int k = 0; k < flds.length; k++ ) {
                if(ignoredeprecated && flds[k].isAnnotationPresent(java.lang.Deprecated.class)) continue;
                String name = flds[k].getName();
                Class type = flds[k].getType();
                int modifiers = flds[k].getModifiers();
                print(Modifier.toString( modifiers) + " ");
                if ( type.isArray() )
                    print(type.getComponentType().getName()
                             + "[] ");
                else
                    print(type.getName() + " ");
                println(name);
            }

            Constructor[] cnsts = clz.getDeclaredConstructors();
            println( "\n...Constructors..." );
            for ( int k = 0; k < cnsts.length; k++ ) {
                if(ignoredeprecated && cnsts[k].isAnnotationPresent(java.lang.Deprecated.class)) continue;
                println(cnsts[k].toString());
/*
                String name = cnsts[k].getName();
                Class params[] = cnsts[k].getParameterTypes();
                int modifiers = cnsts[k].getModifiers();
                print(Modifier.toString( modifiers) +" "
                        + name + "(");
                for (int i = 0; params != null && i < params.length; i++) {
                    if (i != 0)
                        print(", ");

                    Class type = params[i];
                    if ( type.isArray() )
                        print(type.getComponentType().getName()
                               +"[]");
                    else
                        print(type.getName() );
                }
                println(")");
*/
            }

            Method[] mms = clz.getDeclaredMethods();
            println( "\n...Methods..." );
            for (int i = 0; i < mms.length; i++) {
            if(ignoredeprecated && mms[i].isAnnotationPresent(java.lang.Deprecated.class)) continue;

/*
                int md = mms[i].getModifiers();
                Class retType = mms[i].getReturnType();
                print(Modifier.toString(md)+" ");
                if (retType.isArray())
                    print(retType.getComponentType().getName()
                               +"[] ");
                else
                    print(retType.getName() + " ");
                print(mms[i].getName());
                Class cx[] = mms[i].getParameterTypes();
                print("( ");
                if (cx.length > 0) {
                    for (int j = 0; j < cx.length; j++) {
                        if (cx[j].isArray())
                            print(
                                 cx[j].getComponentType().getName() +"[]");
                        else
                            print(cx[j].getName());

                        if (j < (cx.length - 1)) print(", ");
                    }
                }
                print(") ");
*/
                print(mms[i].toString()+" ");
                println("{ }");
            }
            System.out.print(buffer);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

