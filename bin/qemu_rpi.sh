#!/bin/sh

# export QEMU=$(which qemu-system-arm)
# export TMP_DIR=$HOME/tmp/qemu-rpi
# export RPI_KERNEL=${TMP_DIR}/kernel-qemu-4.14.79-stretch
# export RPI_FS=${TMP_DIR}/2018-11-13-raspbian-stretch-lite.img
# export PTB_FILE=${TMP_DIR}/versatile-pb.dtb
# export IMAGE_FILE=2018-11-13-raspbian-stretch-lite.zip
# export IMAGE=http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2018-11-15/${IMAGE_FILE}

# mkdir -p $TMP_DIR; cd $TMP_DIR

# wget https://github.com/dhruvvyas90/qemu-rpi-kernel/blob/master/kernel-qemu-4.14.79-stretch?raw=true \
#        -O ${RPI_KERNEL}

# wget https://github.com/dhruvvyas90/qemu-rpi-kernel/raw/master/versatile-pb.dtb \
# -O ${PTB_FILE}

# wget $IMAGE
# unzip $IMAGE_FILE

QEMU_AUDIO_DRV=none exec /usr/bin/qemu-system-arm \
    -kernel $HOME/kvm/qemu-rpi/kernel-qemu-4.14.79-stretch \
    -cpu arm1176 -m 256 -M versatilepb \
    -dtb $HOME/kvm/qemu-rpi/versatile-pb.dtb -no-reboot \
    -nographic -append "root=/dev/sda2 panic=1 console=ttyAMA0 rootfstype=ext4 rw" \
    -drive "file=$HOME/kvm/qemu-rpi/2018-11-13-raspbian-stretch-lite.img,index=0,media=disk,format=raw,cache=unsafe" \
    -netdev type=tap,id=rpinet,ifname=tap3,script=no \
    -net nic,macaddr=00:00:10:52:22:49,netdev=rpinet \


#    -serial mon:stdio -append "root=/dev/sda2 panic=1 console=ttyAMA0 rootfstype=ext4 rw" \
#    -net user,hostfwd=tcp::3600-:22 -net nic \

