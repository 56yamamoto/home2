#!/bin/sh

QEMU_AUDIO_DRV=none exec /usr/bin/qemu-system-aarch64 \
 -machine virt -cpu cortex-a57 -nographic -m 1024 \
 -kernel $HOME/kvm/vmlinuz-run -initrd $HOME/kvm/initrd-run.img \
 -append "root=/dev/sda2 console=ttyAMA0" \
 -global virtio-blk-device.scsi=off -device virtio-scsi-device,id=scsi \
 -drive file=$HOME/kvm/debian8-arm64.img,id=rootimg,cache=unsafe,if=none \
 -device scsi-hd,drive=rootimg \
 -netdev user,id=n0,dns=8.8.8.8,hostfwd=tcp::5555-:22 \
 -device virtio-net-device,mac=00:00:10:52:22:48,netdev=n0


