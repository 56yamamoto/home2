#!/bin/sh

./configure --host=x86_64-w64-mingw32
make
# TODO: add to DEF= -DSQLITE_ENABLE_COLUMN_METADATA=1 
# TODO: sqlite3.exe is not usable
x86_64-w64-mingw32-gcc -shared -o sqlite3.dll sqlite3.o
strip sqlite3.dll
