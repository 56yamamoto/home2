#!/bin/sh
adb shell rm -rf /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb shell mkdir /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/jd.ijs /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/manifest.ijs /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/api /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/base /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/cd /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/config /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/csv /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/demo /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/doc /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/dynamic /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/gen /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/pm /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/test /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/tutorial /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/dev/jd/types /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd
adb push $HOME/conf.backup/jdkey.txt /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd/config/jdkey.txt
adb push $HOME/dev/jd/cdsrc/android/libs/x86/libjd32.so /data/data/com.jsoftware.j.android/app_jandroid/bin/libjd32.so
adb push $HOME/dev/jd/cdsrc/android/libs/x86/libjd32.so /storage/sdcard0/Android/data/com.jsoftware.j.android/files/addons/data/jd/cd/libjd32.so
