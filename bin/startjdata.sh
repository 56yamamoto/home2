#!/bin/sh
# LC_CTYPE    default locale 
# LC_MESSAGES user locale
# wine bug must use eo_XX.UTF-8 instead of eo.UTF-8
if [ "eo.UTF-8" = ${LC_MESSAGES:-$LANG} ]; then
  export LC_MESSAGES="eo_XX.UTF-8"
fi;
WINEPREFIX="$HOME/.wine" USERPROFILE="C:/users/$LOGNAME" wine32 "H:/b2j/jdata/bin32/jdata.exe" -d
