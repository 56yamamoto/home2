#/bin/sh

cd ~/src/wine-dirs/wine-source
# must install wine32 then wine64
cd ../wine32-build
sudo make install
cd ../wine64-build
sudo make install

cd /usr/local/share
sudo find wine -type d -exec chmod a+rx {} \+
sudo find wine -type f -exec chmod a+r {} \+
cd /usr/local/lib
sudo find wine -type d -exec chmod a+rx {} \+
sudo find wine -type f -exec chmod a+r {} \+
cd /usr/local/lib64
sudo find wine -type d -exec chmod a+rx {} \+
sudo find wine -type f -exec chmod a+r {} \+
