#!/bin/sh

$HOME/bin/j8b0

FROM8=$JOPTHOME/j801/system
FROM=$HOME/jal
TO=$HOME/src/jconsole_for_android/assets

cp -r $FROM8/config $TO/system/.
cp -r $FROM8/defs $TO/system/.
cp -r $FROM8/main $TO/system/.
cp -r $FROM8/util $TO/system/.

cp $HOME/jal/base8/main/config/profile*.ijs $TO/system/../bin/.

cp -r $FROM/addons/api/android $TO/addons/api/.
cp -r $FROM/addons/api/expat $TO/addons/api/.
cp -r $FROM/addons/api/gl3 $TO/addons/api/.
cp -r $FROM/addons/api/gles $TO/addons/api/.
cp -r $FROM/addons/api/jni $TO/addons/api/.
cp -r $FROM/addons/convert/json $TO/addons/convert/.
cp -r $FROM/addons/data/jmf $TO/addons/data/.
cp -r $FROM/addons/data/ddsqlite $TO/addons/data/.
# cp -r $FROM/addons/data/sqlite $TO/addons/data/.
cp -r $FROM/addons/demos/isigraph $TO/addons/demos/.
cp -r $FROM/addons/demos/plot $TO/addons/demos/.
cp -r $FROM/addons/demos/wd $TO/addons/demos/.
cp -r $FROM/addons/demos/wdplot $TO/addons/demos/.
cp -r $FROM/addons/ide/jhs $TO/addons/ide/.
cp -r $FROM/addons/ide/qt $TO/addons/ide/.
cp -r $FROM/addons/general/misc $TO/addons/general/.
cp -r $FROM/addons/general/scriptdoc $TO/addons/general/.
cp -r $FROM/addons/math/misc $TO/addons/math/.
cp -r $FROM/addons/math/deoptim $TO/addons/math/.
cp -r $FROM/addons/games/minesweeper $TO/addons/games/.
cp -r $FROM/addons/games/nurikabe $TO/addons/games/.
cp -r $FROM/addons/games/pousse $TO/addons/games/.
cp -r $FROM/addons/games/solitaire $TO/addons/games/.
cp -r $FROM/addons/graphics/afm $TO/addons/graphics/.
cp -r $FROM/addons/graphics/bmp $TO/addons/graphics/.
cp -r $FROM/addons/graphics/color $TO/addons/graphics/.
cp -r $FROM/addons/graphics/gl2 $TO/addons/graphics/.
cp -r $FROM/addons/graphics/grid $TO/addons/graphics/.
cp -r $FROM/addons/graphics/plot $TO/addons/graphics/.
cp -r $FROM/addons/graphics/print $TO/addons/graphics/.
cp -r $FROM/addons/graphics/treemap $TO/addons/graphics/.
cp -r $FROM/addons/graphics/viewmat $TO/addons/graphics/.
cp -r $FROM/addons/gui/android $TO/addons/gui/.
cp -r $FROM/addons/gui/droidwd $TO/addons/gui/.
cp -r $FROM/addons/gui/wdclass $TO/addons/gui/.
cp -r $FROM/addons/gui/util $TO/addons/gui/.
cp -r $FROM/addons/stats/base $TO/addons/stats/.
cp -r $FROM/addons/tables/csv $TO/addons/tables/.
cp -r $FROM/addons/tables/dsv $TO/addons/tables/.

# rm -rf $TO/addons/api/expat/manifest.ijs
rm -rf $TO/addons/api/gl3/manifest.ijs
rm -rf $TO/addons/api/gles/manifest.ijs
rm -rf $TO/addons/api/jni/manifest.ijs
# rm -rf $TO/addons/gui/android/manifest.ijs
# rm -rf $TO/addons/gui/droidwd/manifest.ijs
rm -rf $TO/addons/data/sqlite/browser.reg
rm -rf $TO/addons/graphics/color/color.jproj
rm -rf $TO/addons/graphics/color/history.txt
rm -rf $TO/addons/graphics/color/run.ijs
rm -rf $TO/addons/ide/jhs/build.ijs
rm -rf $TO/addons/ide/jhs/jgc.ijs
rm -rf $TO/addons/ide/jhs/jgcplot.ijs
rm -rf $TO/addons/ide/jhs/jhs.jproj
rm -rf $TO/addons/ide/jhs/jijxalt.ijs
rm -rf $TO/addons/ide/jhs/jijxdebug.ijs
rm -rf $TO/addons/ide/jhs/jijxmin.ijs
rm -rf $TO/addons/ide/jhs/src/
find $TO/addons/general -name '*.jproj' -delete
find $TO/addons/math -name '*.jproj' -delete
rm -rf $TO/system/config/edit.xml
rm -rf $TO/system/config/j.lang
rm -rf $TO/system/config/term.xml
rm -rf $TO/system/config/view.xml
rm -rf $TO/system/config/winpos.dat
rm -rf $TO/system/defs/hostdefs_aix.ijs
rm -rf $TO/system/defs/hostdefs_darwin.ijs
rm -rf $TO/system/defs/hostdefs_darwin_64.ijs
rm -rf $TO/system/defs/hostdefs_linux.ijs
rm -rf $TO/system/defs/hostdefs_linux_64.ijs
rm -rf $TO/system/defs/hostdefs_sunos.ijs
rm -rf $TO/system/defs/hostdefs_win.ijs
rm -rf $TO/system/defs/hostdefs_win_64.ijs
rm -rf $TO/system/defs/netdefs_aix.ijs
rm -rf $TO/system/defs/netdefs_darwin.ijs
rm -rf $TO/system/defs/netdefs_darwin_64.ijs
rm -rf $TO/system/defs/netdefs_linux.ijs
rm -rf $TO/system/defs/netdefs_linux_64.ijs
rm -rf $TO/system/defs/netdefs_sunos.ijs
rm -rf $TO/system/defs/netdefs_win.ijs
rm -rf $TO/system/defs/netdefs_win_64.ijs

find $OT/addons -iname '*.dll' -delete
find $OT/addons -iname '*.dylib' -delete
find $OT/addons -iname '*.exe' -delete
find $OT/addons -iname '*.so' -delete

mkdir -p $TO/tools/ftp
mkdir -p $TO/tools/zip
cp -f $HOME/android/tools/wget $TO/tools/ftp/.
cp -f $HOME/android/tools/7za $TO/tools/zip/.
cd $TO && tar czf ../jqtdata.tgz * && mv ../jqtdata.tgz ~/dev/apps/ide/jqt/android/assets/.
rm -rf $TO/tools

$HOME/bin/j8b
