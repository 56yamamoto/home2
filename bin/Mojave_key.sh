#!/bin/sh

#Code for Virtualbox:

# cd "C:\Program Files\Oracle\VirtualBox\"
# $ VBoxManage list hostcpuids
# Host CPUIDs:
# Leaf no.  EAX      EBX      ECX      EDX
# 00000000  0000000d 756e6547 6c65746e 49656e69
# 00000001  000306c3 00100800 7ffafbff bfebfbff  <<<<

VBoxManage modifyvm "Mojave" --cpuidset 00000001 000306c3 00100800 7ffafbff bfebfbff
VBoxManage setextradata "Mojave" "VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" "iMac11,3"
VBoxManage setextradata "Mojave" "VBoxInternal/Devices/efi/0/Config/DmiSystemVersion" "1.0"
VBoxManage setextradata "Mojave" "VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" "Iloveapple"
VBoxManage setextradata "Mojave" "VBoxInternal/Devices/smc/0/Config/DeviceKey" "ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"
VBoxManage setextradata "Mojave" "VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC" 1
VBoxManage setextradata "Mojave" "VBoxInternal/CPUM/IsaExts/AVX2" 1

VBoxManage modifyvm "macOS 10.13" --cpuidset 00000001 000306c3 00100800 7ffafbff bfebfbff
VBoxManage setextradata "macOS 10.13" "VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" "iMac11,3"
VBoxManage setextradata "macOS 10.13" "VBoxInternal/Devices/efi/0/Config/DmiSystemVersion" "1.0"
VBoxManage setextradata "macOS 10.13" "VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" "Iloveapple"
VBoxManage setextradata "macOS 10.13" "VBoxInternal/Devices/smc/0/Config/DeviceKey" "ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"
VBoxManage setextradata "macOS 10.13" "VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC" 1
VBoxManage setextradata "macOS 10.13" "VBoxInternal/CPUM/IsaExts/AVX2" 1
