#!/bin/sh

# copy windows binaries from jsrc to jlibrary bin

S=$HOME/dev/jsource/makemsvc
A=$HOME/dev/jsource/jlibrary/bin
B=$HOME/dev/jsource/jlibrary/bin32

cp $S/jconsole/jconsole.exe $A/.
cp $S/jconsole/jconsole32.exe $B/jconsole.exe

cp $S/jdll/javx.dll $A/javx.dll
cp $S/jdll/javx2.dll $A/javx2.dll
cp $S/jdll/j.dll $A/j.dll
cp $S/jdll/j.dll $A/j-nonavx.dll
cp $S/jdll/j32.dll $B/j.dll

cp $S/tsdll/tsdll.dll $A/.
cp $S/tsdll/tsdll32.dll $B/tsdll.dll
