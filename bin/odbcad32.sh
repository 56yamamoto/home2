#!/bin/sh
# LC_CTYPE    default locale 
# LC_MESSAGES user locale
# wine bug must use eo_XX.UTF-8 instead of eo.UTF-8
if [ "eo.UTF-8" = ${LC_MESSAGES:-$LANG} ]; then
  export LC_MESSAGES="eo_XX.UTF-8"
fi;
if [ "x86_64" = `uname -m` ]; then 
  WINEPREFIX="$HOME/.wine" USERPROFILE="C:/users/$LOGNAME" wine32 odbcad32.exe
else
  WINEPREFIX="$HOME/.wine" USERPROFILE="C:/users/$LOGNAME" wine32 odbcad32.exe
fi  
