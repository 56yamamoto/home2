#!/bin/bash
#
# set JAL webhook
#
# $1 is repo, e.g. jsoftware/net_jcs

cd `dirname "$(realpath $0)"`

R=$1

G=https://api.github.com/repos
T=aa19eb5d6c7acab12b8101fbda69bf20f6af5f7f

curl -d "@jgithubhook.json" \
 -H "Authorization: token $T" \
 -H "Content-Type: application/json" \
 -X POST "$G/$R/hooks"
