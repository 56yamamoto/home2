#!/bin/sh
# -------------------------
# modified by Bill Lam <bbill.lam@gmail.com>
# modification
# for TeX Live 2008
# Oct. 10, 2008
#
# Modified on Mar. 6, 2009
# to correct some errors when it is run under Debian Lenny
#
# 1) specify the date format explicitly to avoid the output of Chinese chars
#    under Chinese mode of Debian Lenny
# date "+%m/%d/%y %H:%M:%S"
#
# 2) echo "\endinput" is changed to
#    echo "\\endinput"
#    to avoid the special meaning of "\e" under Debian Lenny's shell
# -------------------------
# to move files and update file(s)
# according to the recommended new TeX Directory Structure (TDS)
#
# any existing .fd .fdx files won't be over-written
#
# in addition, the script can now generate
# the necessary files for vertical typesetting
# *.fdx, *.tfm (in v sub-plane), *.enc (in v sub-plane)
# Sep. 5, 2006
# -------------------------
# (this file contains comment encoded in utf8)
# Check the existence of the target directories
# Move the generated files to the proper places
# May 10, 2005
# -------------------------
# disabling anything related to type 1; 
# focusing only on unicode-based TTF
# making UTF8 the only option for the target encoding
#
# That is, this script is mainly used to generate
# *.tfm, *.enc, *.fd, *.map files
# for unicode-based TTF fonts to be used in CJK UTF8 environment
# Similar to what cyberb.sh does;
#   but additionally generate *.fd file 
#   (cyberb.sh does not generate c70song.fd; CJK has this built-in)
# Apr 11, 2005
# -------------------------
# original 'mkfont.sh' is created
# by Edward G.J. Lee <edt1023@info.sayya.org>
# This code is Public Domain.
#
# $Id: mkfont.sh,v 1.4 2004/07/16 09:54:47 edt1023 Exp $
#

clear

# to know where I am

location_path=`dirname "$0"`
if [ "$location_path" = '.' ]
then
	location_path=`pwd`
fi

cd
home_dir=`pwd`
cd "$location_path"


# to locate the possible ttf file
#TEMPNAME=`ls -m "$location_path"/*.ttf`
#echo $TEMPNAME | awk -F, '{printf "FULLNAME=%s",\$1}'
#eval `echo $TEMPNAME | awk -F',' '{printf "FULLNAME='%s'",\$1}'`
#echo "${FULLNAME}"
#exit

#clear

# ask ttf font file names and CJK naming scheme
echo "ttf file name = "
read FULLNAME

echo "font name in TeX/LaTeX to be ="
read FONTNAME1

# check its existence in TeX
# adapted from
# 'install.sh'
# in nsung-1.4.2
# by Edward Lee

kpsewhich ${FONTNAME1}.map > /dev/null
# there will be output (path) to the file
# if it exists
if [ $? = 0 ]; then
	echo
	echo "TeX already has ${FONTNAME1}. abort!"
	echo
#	exit
fi

# also check its variants 
# (convention for the real font in unicode encoding)

kpsewhich ${FONTNAME1}u.map > /dev/null
# there will be output (path) to the file
# if it exists
if [ $? = 0 ]; then
	echo
	echo "TeX already has ${FONTNAME1}u."
	echo "  this will conflict the operation of this script."
	echo "  Abort!"
	echo
#	exit
fi


echo "font name in CJK to be (can be set to ${FONTNAME1}) = "
read FONTNAME2

#
# locations (relative to [texmf])
TFMLOC='/fonts/tfm/CJK/'${FONTNAME1}
ENCLOC='/fonts/enc/CJK/'${FONTNAME1}
FONTMAPLOC='/fonts/map/pdftex/CJK'
TTFLOC='/fonts/truetype/CJK'
CJKLOC='/tex/latex/CJK/UTF8'
CIDXMAP='/fonts/map/dvipdfm/dvipdfmx'
#DVIPDFMXCFG='/dvipdfm/config'
#TTFONTSMAP='/fonts/map/ttf2pk/config'



DATE=`date "+%m/%d/%y %H:%M:%S"`

FONTNAME0=`basename $FULLNAME`
eval `echo $FONTNAME0 | awk -F. '{printf "FHEAD=%s;FTAIL=%s",\$1,\$2}'`

# FULLNAME : may include path
# FONTNAME0 : FULLNAME stripped the leading path; bkai00mp.ttf
# FHEAD : FONTNAME0 w/o extension name (FTAIL); bkai00mp
# FONTNAME1 : used in TeX; will be appended by 'u' for UTF8
#   when it is used in the file names (*.tfm, *.enc); bkaiu
#   no appending when it is used in the sub-directory name; bkai
# FONTNAME2 : only used in CJK package; bkai


check_ttf()
{
  if [ ! -f "$FULLNAME" ]
  then
    echo
    echo "TTF File doesn't exist!"
    echo
    exit
  fi
    
  if [ "$FTAIL" != "ttf" ] && \
     [ "$FTAIL" != "TTF" ] && \
     [ "$FTAIL" != "ttc" ] && \
     [ "$FTAIL" != "TTC" ]
  then
    echo
    echo "TTF File is not a TrueType font."
    echo
    exit
  fi
}

check_enc()
{
#
# [npc] the following defnitions of NUMLIST, CJKENC
# are adapted from cyberb.sh
# by Edward Lee
# for UTF8 encoding
# NUMLIST=`awk 'BEGIN{n=0; while(n<256){printf "%02x\n",n; n++}}'`
 CJKENC=70
}

# [npc]the following is adapted from cycberb.sh
# it will use the latest Unicode.sfd file
# from the TexLive distribution in
# [texmf]/fonts/sfd/
create_tfm()
{
 ttf2tfm $FONTNAME0 -w ${FONTNAME1}u'@Unicode@'
}

# [npc] the following is adapted from 
# <http://blog.bs2.to/post/EdwardLee/7672>
# 
# the *.map file will be stored in
# [texmf]/fonts/map/pdftex/
 
build_map()
{
 MAPFILE=${FONTNAME1}u.map
# this is the old code segment
# it will generate 256 entries
# no matter whether the font has it or not
# for i in $NUMLIST
# do
#   cat >> $MAPFILE << EOF
#$FHEAD${i} <$FHEAD${i}.enc <$FONTNAME0 
#EOF
# done

# the new code only generate the entries with 
# the corresponding .enc files
# (generated by ttf2tfm)

for i in *.tfm
do
cat >> $MAPFILE << EOF
${i%.tfm} <${FONTNAME0} <${i%.tfm}.enc 
EOF
done
}


# based on the entry for cyberbit 
# in the cid-x.map included in dvipdfmx source
create_cidmap()
{
cat >> cid-x.entry << EndOfFile
 
% added by Bill Lam <bbill.lam@gmail.com>
% $DATE
%
${FONTNAME1}u@Unicode@ unicode :0:$FONTNAME0
EndOfFile
}



# based on the c70song.fd in CJK macro
create_cjkfd()
{
cat > c${CJKENC}${FONTNAME2}.fd << EndOfFile
% This is c${CJKENC}${FONTNAME2}.fd for CJK package.
% created by Bill Lam <bbill.lam@gmail.com>
% with mkfontcjk.sh
% $DATE
%
\def\fileversion{4.7.0}
\def\filedate{$DATE}
\ProvidesFile{c${CJKENC}${FONTNAME2}.fd}[\filedate\space\fileversion]

% character set: Unicode U+0080 - U+FFFD
% font encoding: Unicode

\DeclareFontFamily{C${CJKENC}}{$FONTNAME2}{\hyphenchar \font\m@ne}
\DeclareFontShape{C${CJKENC}}{$FONTNAME2}{m}{n}{<-> CJK * ${FONTNAME1}u}{}
%\DeclareFontShape{C${CJKENC}}{$FONTNAME2}{m}{sl}{<-> CJK * ${FONTNAME1}us}{}
%\DeclareFontShape{C${CJKENC}}{$FONTNAME2}{m}{it}{<-> CJKssub * ${FONTNAME2}/m/sl}{}
\DeclareFontShape{C${CJKENC}}{$FONTNAME2}{bx}{n}{<-> CJKb * ${FONTNAME1}u}{\CJKbold}
%\DeclareFontShape{C${CJKENC}}{$FONTNAME2}{bx}{sl}{<-> CJKb * ${FONTNAME1}us}{\CJKbold}
%\DeclareFontShape{C${CJKENC}}{$FONTNAME2}{bx}{it}{<-> CJKssub * ${FONTNAME2}/bx/sl}{\CJKbold}
\endinput
EndOfFile
}

# based on the c70bsmi.fdx in CJK macro package
# only generate the header
# the rest of the file will be generated
# in another process
create_cjkfdx()
{
cat > c${CJKENC}${FONTNAME2}.fdx << EndOfFile
% This is c${CJKENC}${FONTNAME2}.fdx for CJK package.
% created by Bill Lam <bbill.lam@gmail.com>
% for vertical typesetting
% $DATE
% 
\def\fileversion{4.7.0}
\def\filedate{$DATE}
\ProvidesFile{c${CJKENC}${FONTNAME2}.fdx}[\filedate\space\fileversion]

\CJKvdef{fullheight}{1em}
\CJKvdef{height}{.88em}
\CJKvdef{offset}{.6em}

EndOfFile
}

#########################
#main()
check_ttf
check_enc
echo "####### create tfm #######"
create_tfm
echo "####### build map #######"
build_map
echo "####### create cid-x.map entry #######"
create_cidmap
echo "####### create fd #######"
create_cjkfd
echo "####### create generic fdx #######"
create_cjkfdx
echo "####### organizaing files into temp folders#######"
TFM=${FONTNAME1}-tfm
ENC=${FONTNAME1}-enc
mkdir -p $TFM $ENC
mv -f *.enc $ENC
mv -f *.tfm $TFM
#################
# generate tfm and enc files for 'v' plane subfont
# for vertical typesetting
# generate more content for .fdx file
# following Werner Lemberg's instruction at
# <http://lists.ffii.org/pipermail/cjk/2005-September/001264.html>
#
echo "#################"
echo "generate files for vertical type-setting"
echo
cd mkfontcjk_files
fontforge -script vertical.pe ../$FONTNAME0 ${FONTNAME1}uv
fontforge -script vertref.pe ../$FONTNAME0 ${FONTNAME1}uvr
perl makefdx.pl ${FONTNAME1}uvr.afm Unicode.sfd temp.fdx

# temp.fdx only contains
# something like
# \CJKvdef{m/n/00/175}{\def\CJK@plane{v}\selectfont\CJKsymbol{0}}
# we need also 
# something like
# \CJKvlet{bx/n/00/175}{m/n/00/175}
./makeCJKvlet.py temp.fdx temp2.fdx

cd ..
cat mkfontcjk_files/temp.fdx >> c${CJKENC}${FONTNAME2}.fdx
cat mkfontcjk_files/temp2.fdx >> c${CJKENC}${FONTNAME2}.fdx
echo "\\endinput" >> c${CJKENC}${FONTNAME2}.fdx

# modify .map to include 'v' plane subfont for vertical typesetting
cat >> ${FONTNAME1}u.map << EndOfFile
${FONTNAME1}uv <${FONTNAME0} <${FONTNAME1}uv.enc
EndOfFile

# afm and pfb files are not needed any more
# including *v.afm, *vr.afm, *v.pfb, *vr.pfb
rm mkfontcjk_files/${FONTNAME1}uv*.afm
rm mkfontcjk_files/${FONTNAME1}uv*.pfb

# move *v.enc and *v.tfm files
mv mkfontcjk_files/${FONTNAME1}uv.enc $ENC
mv mkfontcjk_files/${FONTNAME1}uv.tfm $TFM

# *vr.enc and *vr.tfm are not needed
rm mkfontcjk_files/${FONTNAME1}uvr.enc
rm mkfontcjk_files/${FONTNAME1}uvr.tfm

# temp.fdx and temp2.fdx are not needed any more
rm mkfontcjk_files/temp*.fdx
##################

echo "##############################"
echo 
echo "This is what to do next:"
echo "Move the following files to:"
echo "  ${FONTNAME1}-enc/*.enc to [texmf]${ENCLOC}/"
echo "  ${FONTNAME1}-tfm/*.tfm to [texmf]${TFMLOC}/"
echo "  ${FONTNAME1}u.map to [texmf]${FONTMAPLOC}/"
echo "  ${FONTNAME0} to [texmf]${TTFLOC}/"
echo "  c${CJKENC}${FONTNAME2}.fd to [texmf]${CJKLOC}/"
echo "  c${CJKENC}${FONTNAME2}.fdx to [texmf]${CJKLOC}/"
echo "  append cid-x.map to [texmf]$CIDXMAP/cid-x.map"
echo "execute 'sudo texhash'"
echo "execute 'sudo updmap-sys --enable Map ${FONTNAME1}u.map'"
echo
echo 'During the process, this script will need administrator privilege'
echo '   if it has to create any of the sub-directory listed above'
echo
echo 'Also, it needs the privilege to copy files to those directories.'
echo 'The system will ask for administrator password.'
echo 
echo "Do you want this shell script to move the files to the proper places? (Yes/No)"
echo "Type Y or N as your answer and hit RETURN key."
read answer_choice
#
if [ $answer_choice = "N" -o $answer_choice = "n" ]
then
    echo 'OK.  The script will not move the files for you.'
    exit
fi
echo "##############################"
echo "Different TeX systems may have different arrangement for [texmf]"
echo "For example, TeX Live 2008 on Mac OS X has"
echo "  [texmf]=/usr/local/texlive/texmf-local"
echo "  [texmf]=~/texmf"
echo "  for system-wide local or peronal modification."
echo
#
# ask for [texmf] and check the existence of [texmf]
while [  1 ]
do
  echo '1) [texmf]= ~/texmf'
  echo '2) [texmf]= /usr/local/texlive/texmf-local'
  echo '   [texmf]= other choice'
  echo 'Please enter 1 or 2 or path and hit RETURN key:' 
  read ans_choice
  if [ "$ans_choice" = '1' ]
  then
    SUDO = ""
    texmf_choice="$home_dir/texmf"
  elif [ "$ans_choice" = '2' ]
  then
    SUDO = "sudo"
    texmf_choice='/usr/local/texlive/texmf-local'
  else
    SUDO = "sudo"
    texmf_choice="$ans_choice"
  fi

  if ! [ -d "$texmf_choice" ]
  then
    echo "$texmf_choice does not exist."
    echo 'please check the spelling.'
    echo 'or press control-C to terminate the program.'
    echo
  else
    echo "$texmf_choice exists.  Good."
    break
  fi
done
#

# mkdir -p can create the intermidiate directories as needed.

# check the existence of [texmf]/fonts/enc/CJK/$FONTNAME1
# to host *.enc files
echo "##############################"
if ! [ -d ${texmf_choice}${ENCLOC} ]
then
    echo "${texmf_choice}${ENCLOC} does not exist.  This script will create it."
    $SUDO mkdir -p ${texmf_choice}${ENCLOC}
else
    echo "${texmf_choice}${ENCLOC} exists."
    echo "Your tex system already has this font.  Stopped."
    exit
fi


# check the existence of [texmf]/fonts/tfm/CJK/$FONTNAME1
# to host *.tfm files
echo "##############################"
if ! [ -d ${texmf_choice}${TFMLOC} ]
then
    echo "${texmf_choice}${TFMLOC} does not exist.  This script will create it."
    $SUDO mkdir -p ${texmf_choice}${TFMLOC}
else
    echo "${texmf_choice}${TFMLOC} exists."
    echo "Your tex system already has this font.  Stopped."
    exit
fi

# check the existence of [texmf]/fonts/map/pdftex/CJK
# to host ${FONTNAME1}u.map file
echo "##############################"
if ! [ -d ${texmf_choice}${FONTMAPLOC} ]
then
	echo "${texmf_choice}${FONTMAPLOC} does not exist.  This script will create it."
	$SUDO mkdir -p ${texmf_choice}${FONTMAPLOC}
else
	echo "${texmf_choice}${FONTMAPLOC} exists.  Good."
fi



# check the existence of [texmf]/fonts/map/dvipdfm/dvipdfmx
# to host cid-x.map
# as of Mar. 7, 2008, for some unknown reason in TeX Live distribution
# cid-x.map is located in 
# /usr/local/texlive/2008/texmf/fonts/map/dvipdfm/dvipdfmx
#
# As a result, it has higher priority than that in
# /usr/local/texlive/texmf-local/fonts/map/dvipdfm/dvipdfmx
# So, to make it effective, its [texmf] has to be one of the followings:
# [texmf]= /usr/local/texlive/2008/texmf
# or
# [texmf]=~/texmf
#
# To work around the bug, the script will check
# kpsewhich cid-x.map
# If it shows 
#   /usr/local/texlive/2008/texmf/fonts/map/dvipdfm/dvipdfmx/cid-x.map
# then it contains the bug and the script will update both locations
# [texmf]=/usr/local/texlive/2008/texmf
# and
# [texmf]=/usr/local/texlive/texmf-local
# if the system-wide installation is requested.
#
# At first time of installing fonts, the location
#   [texmf]=/usr/local/texlive/texmf-local
# does not contain cid-x.map.  The script will duplicate a copy from
#   [texmf]=/usr/local/texlive/2008/texmf
echo "##############################"
# the work-around for the bug
#if [ "$texmf_choice" = "/usr/local/texlive/texmf-local" ]
#then
#	cidxmap_texmf='/usr/local/texlive/2008/texmf'
#else
#	cidxmap_texmf=${texmf_choice}
#fi

# check the existence of the path
if ! [ -d ${texmf_choice}${CIDXMAP} ]
then
	echo "${texmf_choice}${CIDXMAP} does not exist.  This script will create it."
	$SUDO mkdir -p ${texmf_choice}${CIDXMAP}
else
	echo "${texmf_choice}${CIDXMAP} exists.  Good."
fi

# check if the file cid-x.map exists
cidxpath=`kpsewhich cid-x.map`

if ! [ "$cidxpath" = '' ]
then
	# non-empty string; cid-x.map exists
	cidxmap_exist=1
else
	# emtpy string; cid-x.map does NOT exists
	# this should not happen in TeXLive 2008 distribution
	cidxmap_exist=0
fi


# If the path begins with /usr and contains texmf/fonts then the whole line will be output
sed_output=`echo $cidxpath |sed -n "/^\/usr/p" |sed -n "/texmf\/fonts/p"`
if ! [ "$sed_output" = '' ]
then
	# non-empty string; the path HAS keyword.
	# cid-x.map is in [texmf]=/usr/local/texlive/2008/texmf
	cidxmap_location_bug=1
else
	# empty string; the path has NO keyword.
	# cid-x.map is at other location:
	# Maybe it is at [texmf]=~/texmf
	# the bug will not cause any trouble
	#
	# Maybe it is at [texmf]=/usr/local/texlive/texmf-local
	#   ---> this means bug is fixed!
	# or, it does not exists at all.  
	#   This will be taken care of in the copying sage.
	cidxmap_location_bug=0
fi



# check the existence of [texmf]/fonts/truetype/CJK
# to host ${FONTNAME0}
echo "##############################"
if ! [ -d ${texmf_choice}${TTFLOC} ]
then
    echo "${texmf_choice}${TTFLOC} does not exist.  This script will create it."
    $SUDO mkdir -p ${texmf_choice}${TTFLOC}
else
    echo "${texmf_choice}${TTFLOC} exists.  Good."
fi



# check the existence of [texmf]/tex/latex/CJK/UTF8
echo "##############################"
if ! [ -d ${texmf_choice}${CJKLOC} ]
then
    echo "${texmf_choice}${CJKLOC} does not exist.  This script will create it."
    $SUDO mkdir -p ${texmf_choice}${CJKLOC}
else
    echo "${texmf_choice}${CJKLOC} exists.  Good."
fi



# begin to move / modify files
echo
echo "##############################"
echo "Begin to move files..."
echo "  *.enc"
$SUDO mv ${FONTNAME1}-enc/*.enc ${texmf_choice}${ENCLOC}/
echo "  *.tfm"
$SUDO mv ${FONTNAME1}-tfm/*.tfm ${texmf_choice}${TFMLOC}/
echo "  ${FONTNAME1}u.map"
$SUDO mv ${FONTNAME1}u.map ${texmf_choice}${FONTMAPLOC}/
# echo "copy ${FONTNAME0}"
# sudo cp ${FONTNAME0} ${texmf_choice}${TTFLOC}/
echo "copy ${FULLNAME}"
$SUDO cp ${FULLNAME} ${texmf_choice}${TTFLOC}/
# to avoid over-writing built-in .fd and .fdx files
# c70bkai.fd, c70bsmi.fd, c70gkai.fd, c70gbsn.fd 
# and their .fdx files
# They are already in CJK package
# They corresponds to
# bkai00mp.ttf, bsmi00lp.ttf, gkai00mp.ttf, gbsn00lp.ttf
#
if [ -f ${texmf_choice}${CJKLOC}/c${CJKENC}${FONTNAME2}.fd ]
then
# .fd exists; rename it to be back-up copies
	echo "c${CJKENC}${FONTNAME2}.fd exists.  To be renamed for achive."
	$SUDO mv ${texmf_choice}${CJKLOC}/c${CJKENC}${FONTNAME2}.fd ${texmf_choice}${CJKLOC}/c${CJKENC}${FONTNAME2}.fd_bkup
fi

if [ -f ${texmf_choice}${CJKLOC}/c${CJKENC}${FONTNAME2}.fdx ]
then
# .fdx exists; rename it to be back-up copies
	echo "c${CJKENC}${FONTNAME2}.fdx exist.  To be renamed for achive."
	$SUDO mv ${texmf_choice}${CJKLOC}/c${CJKENC}${FONTNAME2}.fdx ${texmf_choice}${CJKLOC}/c${CJKENC}${FONTNAME2}.fdx_bkup
fi


echo "move c${CJKENC}${FONTNAME2}.fd and c${CJKENC}${FONTNAME2}.fdx"
$SUDO mv c${CJKENC}${FONTNAME2}.fd ${texmf_choice}${CJKLOC}/
$SUDO mv c${CJKENC}${FONTNAME2}.fdx ${texmf_choice}${CJKLOC}/

# end of moving

# add entries into cid-x.map, if it exists
# if not, move the new cid-x.map file to 
# the proper place [texmf]/fonts/map/dvipdfm/dvipdfmx/

echo "##############################"

# TeXLive 2008 has cid-x.map at
# [texmf]=/usr/local/texlive/2008/texmf
# [texmf]/fonts/map/dvipdfm/dvipdfmx/cid-x.map
# Duplicate a copy to the location
#   [texmf]=/usr/local/texlive/texmf-local
#   if it is not there.

if [ "$cidxmap_exist" = 1 ]
then
	# the system has cid-x.map
	if ! [ -f ${texmf_choice}${CIDXMAP}/cid-x.map ]
	then
		# but it is not at texmf_choice
		# it is possible in the following two cases
		# 1) we want it to be home-texmf this time
		# --> duplicate a copy from the system
		# 2) the first time of font installation, 
		# cid-x.map is at texmf
		# but texmf_choice is texmf-local
		# --> duplicate a copy from the system
		echo "sudo cp ${cidxpath} ${texmf_choice}${CIDXMAP}/cid-x.map"
		$SUDO cp ${cidxpath} ${texmf_choice}${CIDXMAP}/cid-x.map
	fi
	# there is cid-x.map at texmf_choice now.
	# pre-existing, or newly copied
	# update it
    echo "This script will append the new result to cid-x.map."
    #
    cp ${texmf_choice}${CIDXMAP}/cid-x.map ./temp_cid_x.map
    cat ./cid-x.entry >> ./temp_cid_x.map
    $SUDO rm ${texmf_choice}${CIDXMAP}/cid-x.map
   	$SUDO mv ./temp_cid_x.map ${texmf_choice}${CIDXMAP}/cid-x.map
    rm ./cid-x.entry
	#
	# if texmf_choice is texmf-local
	# and cidxmap_location_bug=1
	# we need to maintain another copy at texmf
	if [ "$cidxmap_location_bug" = 1 ] && [ "$texmf_choice" = "/usr/local/texlive/texmf-local" ]
	then
		# remove the old one at texmf
		echo "sudo rm ${cidxpath}"
		sudo rm ${cidxpath}
		# copy the new one at texmf-local to texmf
		echo "sudo cp ${texmf_choice}${CIDXMAP}/cid-x.map ${cidxpath}"
		sudo cp ${texmf_choice}${CIDXMAP}/cid-x.map ${cidxpath}
	fi
else
	# the system has no cid-x.map at all
	# this should not happen in TeXLive2008 distribution
	# That's OK.  Create one from scratch.
	# move the newly created cid-x.map to the proper place
	echo "sudo mv ./cid-x.map ${texmf_choice}${CIDXMAP}/cid-x.map"
	$SUDO mv ./cid-x.map ${texmf_choice}${CIDXMAP}/cid-x.map
fi
###########################




rmdir ${FONTNAME1}-enc
rmdir ${FONTNAME1}-tfm


echo "##############################"
echo "To update the TeX file trees and map files..."
echo "It will take some time..."
echo
sudo texhash
sudo updmap-sys --enable Map ${FONTNAME1}u.map

if [ "$texmf_choice" = "$home_dir/texmf" ]
# personal location needs an additional step
then
	sudo updmap --enable Map ${FONTNAME1}u.map
fi

echo "##############################"
echo "To work around the bug in dvipdfm.map"
echo "Now the python script fix_dvipdfm_map.py will correct the problem if necessary."
cd mkfontcjk_files
sudo ./fix_dvipdfm_map.py
cd ..

echo 
echo "##############################"
echo 'DONE.'
echo "##############################"
echo "This terminal window can be closed now.  Thank you."
