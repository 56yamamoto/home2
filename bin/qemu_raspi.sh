#!/bin/sh

QEMU_AUDIO_DRV=none exec /usr/bin/qemu-system-arm \
 -machine versatilepb -cpu arm1176 -nographic -m 256 \
 -no-reboot \
 -append "root=/dev/sda2 panic=1 console=ttyAMA0 rootfstype=ext4 rw" \
 -kernel $HOME/kvm/kernel-qemu-3.10.25-wheezy \
 -drive format=raw,file=$HOME/kvm/2012-12-16-wheezy-raspbian.img,cache=unsafe \
 -netdev type=tap,id=rpinet,ifname=tap3,script=no \
 -net nic,macaddr=00:00:10:52:22:49,netdev=rpinet \


# -net user,hostfwd=tcp::3600-:22 -net nic \
# -net nic -net user \
# -netdev tap,id=mmnet,ifname=tap3,script=no \

