#!/bin/sh

# copy binaries from jlibrary to share/jsoftware

A=$HOME/dev/jsource/jlibrary
B=$HOME/share/jsoftware/j902
C=$HOME/share/ins.d/j902-win

function cop(){
# $1 dir
# $2 lib
# MUST rename/remove object first; overwrite cause cache error
if [ -f "$A/$1/$2" ]; then
if [ -f "$B/$1/$2" ]; then
mv "$B/$1/$2" "$B/$1/$2.old"
fi
cp "$A/$1/$2" "$B/$1/$2"
cp "$A/$1/$2" "$C/$1/$2"
fi
}

cop bin jconsole
cop bin jconsole-lx
cop bin jconsole-mac
cop bin jconsole.exe

cop bin libj.so
cop bin libj-nonavx.so
cop bin libjavx.so
cop bin libjavx2.so

cop bin libj.dylib
cop bin libj-nonavx.dylib
cop bin libjavx.dylib
cop bin libjavx2.dylib

cop bin j.dll
cop bin j-nonavx.dll
cop bin javx.dll
cop bin javx2.dll

cop bin32 jconsole
cop bin32 jconsole.exe

cop bin32 libj.so
cop bin32 libj.dylib
cop bin32 j.dll

cp $A/bin/j-nonavx.dll $C/bin/j.dll || true
