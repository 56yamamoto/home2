#!/bin/sh
cd ~/dev/jsource || exit 1
cp bin/linux/j64avx2/libj.so ~/share/jsoftware/j903/bin/.
cp bin/linux/j64avx2/libj.so ~/share/jsoftware/j903/bin/libjavx2.so
cp bin/linux/j64avx/libj.so ~/share/jsoftware/j903/bin/libjavx.so
cp bin/linux/j64/libj.so ~/share/jsoftware/j903/bin/libj-nonavx.so
cp bin/linux/j32/libj.so ~/share/jsoftware/j903/bin32/.
cp makemsvc/jdll/javx2.dll ~/share/jsoftware/j903/bin/j.dll
cp makemsvc/jdll/javx2.dll ~/share/jsoftware/j903/bin/javx2.dll
cp makemsvc/jdll/javx.dll ~/share/jsoftware/j903/bin/javx.dll
cp makemsvc/jdll/j.dll ~/share/jsoftware/j903/bin/j-nonavx.dll
cp makemsvc/jdll/j32.dll ~/share/jsoftware/j903/bin32/j.dll
