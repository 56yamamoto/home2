#!/bin/sh

#Code for Virtualbox:

# cd "C:\Program Files\Oracle\VirtualBox\"
# $ VBoxManage list hostcpuids
# Host CPUIDs:
# Leaf no.  EAX      EBX      ECX      EDX
# 00000000  0000000d 756e6547 6c65746e 49656e69
# 00000001  000306c3 00100800 7ffafbff bfebfbff  <<<<

VBoxManage modifyvm "macOSB" --cpuidset 00000001 000306c3 00100800 7ffafbff bfebfbff
VBoxManage setextradata "macOSB" "VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" "iMac19,1"
VBoxManage setextradata "macOSB" "VBoxInternal/Devices/efi/0/Config/DmiSystemVersion" "1.0"
VBoxManage setextradata "macOSB" "VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" "Mac-AA95B1DDAB278B95"
# VBoxManage setextradata "macOSB" "VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" "Iloveapple"
VBoxManage setextradata "macOSB" "VBoxInternal/Devices/smc/0/Config/DeviceKey" "ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"
VBoxManage setextradata "macOSB" "VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC" 1
VBoxManage setextradata "macOSB" "VBoxInternal/CPUM/IsaExts/AVX2" 1
