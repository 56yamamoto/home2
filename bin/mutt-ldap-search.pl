#!/usr/bin/perl

# use strict;
use Net::LDAP;

use constant HOST => 'localhost';
use constant BASE => 'ou=Contacts, o=b2j';
use constant VERSION => 3;
use constant SCOPE => 'sub';

my $name;
my @attributes = qw( dn givenName sn mail );
{
    print "Searching directory... ";
    $name = shift || die;
    my $filter = "(|(sn=$name*)(givenName=$name*))";
    my $ldap = Net::LDAP->new( HOST, onerror => 'die' )
            || die "Cannot connect: $@";

#    $ldap->bind(version => VERSION) or die "Cannot bind: $@";
     $ldap->bind( "uid=bill,ou=People,o=b2j", password => "--PASSWD--" ) or die "Cannot bind: $@";

    my $result = $ldap->search( base => BASE,
                            scope => SCOPE,
                            attrs => \@attributes,
                            filter => $filter
                            );

    my @entries = $result->entries;

    $ldap->unbind();

    print scalar @entries, " entries found.\n";

    foreach my $entry ( @entries ) {
        my @emailAddr = $entry->get_value('mail');
        foreach my $addr (@emailAddr) {
            print $addr , "\t";

            if ($entry->get_value('sn') eq '.') {
             print $entry->get_value('givenName'), "\n";
            } else {
             print $entry->get_value('givenName'), " ";
             print $entry->get_value('sn'), "\n";
            }

        }
    }
}

