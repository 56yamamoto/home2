#!/bin/sh
# set up keyboard to exchange the Shift and End key
if [ "debian2" = `hostname` ]; then
  xmodmap -e "remove Shift = Shift_R"
  xmodmap -e "keycode 103 = Shift_R" # End         => Shift
  xmodmap -e "keycode 62 = End"      # Shift_R     => End
  xmodmap -e "add shift = Shift_R"    # Make the new Shift key actually do shifting
fi
