#/bin/sh

# fall back to wine-4.21  sha1: 2935bab96569dcd734a8e85
# wine-5 not working

cd ~/src/wine-source
cd ../wine64-build
../wine-source/configure --enable-win64 --disable-tests && make clean && make
cd ../wine32-build
PKG_CONFIG_PATH=/usr/lib/pkgconfig ../wine-source/configure --with-wine64=../wine64-build --without-krb5 --disable-tests && make clean && make

echo "call winemakeins.sh"
