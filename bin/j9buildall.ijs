NB. build all addons jdb pubish
cocurrent 'base'

ignorehash=: 0

F1=: <;._2 (0 : 0)
api/expat/source/build.ijs
api/gles/source/build.ijs
api/jni/source/build.ijs
api/ncurses/build.ijs
arc/ziptrees/build.ijs
arc/zlib/source/build.ijs
convert/jiconv/build.ijs
convert/json/source/build.ijs
convert/bjson/source/build.ijs
convert/pjson/source/build.ijs
data/ddmysql/source/build.ijs
data/ddsqlite/source/build.ijs
data/sqlite/source/save/build.ijs
data/jfiles/source/build.ijs
data/jmf/source/build.ijs
data/odbc/source/build.ijs
data/sqlite/source/save/build.ijs
data/sqltable/source/build.ijs
demos/coins/build.ijs
demos/isigraph/build.ijs
demos/qtdemo/build.ijs
demos/wd/build.ijs
demos/wd/source/save/build.ijs
demos/wd/source/webview/base/build.ijs
demos/wdplot/build.ijs
docs/joxygen/source/build.ijs
finance/actuarial/build.ijs
finance/interest/build.ijs
format/publish/source/base/save/build.ijs
games/2048/build.ijs
games/minesweeper/build.ijs
games/nurikabe/build.ijs
games/pousse/build.ijs
games/solitaire/build.ijs
general/dirtrees/build.ijs
general/dirutils/build.ijs
general/inifiles/source/build.ijs
graphics/afm/source/afm/build.ijs
graphics/afm/source/afmdev/build.ijs
graphics/bmp/source/build.ijs
graphics/cairo/source/save/build.ijs
graphics/gnuplot/build.ijs
graphics/graph/source/build.ijs
graphics/jpeg/source/build.ijs
# graphics/pdfdraw/publish/build.ijs
graphics/pdfdraw/source/save/build.ijs
graphics/plot/source/base/save/build.ijs
graphics/png/source/build.ijs
graphics/print/source/build.ijs
graphics/treemap/build.ijs
graphics/viewmat/source/build.ijs
ide/ja/source/save/build.ijs
ide/jhs/build.ijs
# ide/jnet/source/save/build.ijs
ide/qt/source/save/build.ijs
labs/labs/build.ijs
labs/labs/source/run/build.ijs
labs/labs/source/run805/build.ijs
# math/cal/source/build.ijs
math/deoptim/source/build.ijs
math/eigenpic/source/build.ijs
math/fftw/build.ijs
math/lapack/source/build.ijs
math/lapack2/source/build.ijs
math/lbfgs/source/save/build.ijs
math/misc/build.ijs
math/mt/build.ijs
# math/tabula/source/save/build.ijs
# math/uu/source/build.ijs
net/clientserver/build.ijs
net/jcs/build.ijs
net/websocket/source/save/build.ijs
net/zmq/build.ijs
stats/base/build.ijs
stats/distribs/normal/build.ijs
stats/r/save/build.ijs
stats/jserver4r/source/base/build.ijs
stats/rlibrary/splines/build.ijs
tables/csv/source/build.ijs
tables/dsv/source/build.ijs
tables/excel/source/build.ijs
tables/tara/source/build.ijs
tables/taraxml/source/build.ijs
tables/wdooo/source/build.ijs
types/datetime/source/build.ijs
web/gethttp/build.ijs
)

f1=: 3 : 0
smoutput y
if. '#'={.y do.
  if. -.ignorehash do. i.0 0 return. end.
  y=. }.y
end.
load '~Addons/', y
i.0 0
)

load 'project'

NB. uncomment to retain comment for comparison with J6
NB. writesourcex_jp_ =: writesource_jp_

f1 &.> F1
load '~JNet/save/build.ijs'
