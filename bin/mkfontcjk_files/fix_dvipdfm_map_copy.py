#! /usr/bin/python
# For The TexLive 2008 issued on Feb. 2008
# Its updmap-sys has a bug, which prevents the command to generate
# a proper dvipdfm_dl14.map,
# with respect to Chinese TrueType font map (originally for pdftex)
# updmap-sys propagates the font map info into dvipdfm_dl14.map
# with incorrect syntax
#
# the entry of the incorrect syntax:
# bkaiu00 bkaiu00 <bkai00mp.ttf
# 
# this script will scan the active dvipdfm_dl14.map and dvipdfm_ndl14.map
#   and look for the specified Chinese TrueType font entry
#
# modify them into
# bkaiu00 bkaiu00
#
# usage syntax:
# fix_dvipdfm_map.py font_name
#
# for example,
# fix_dvipdfm_map.py bkaiu
# 
# written by
# Nien-Po Chen  <npchen@saturn.yzu.edu.tw>
# Apr. 17, 2008

# the following path may need modification in different platforms
# so that /usr/texbin/kpsewhich execute the command 'kpsewhich'
# texbin='/usr/texbin/'
texbin='/usr/local/texlive/2008/bin/x86_64-linux/'

# the command 'kpsewhich'
kp='kpsewhich'

# the name of the file to be fixed
fixfn=['dvipdfm_dl14.map','dvipdfm_ndl14.map']

import os, sys, string, fileinput

font_name = sys.argv[1]
#font_name = 'bkaiu'


cmd_fid=os.popen(texbin + kp + ' ' + font_name + '.map')
cmd_output=cmd_fid.readline()
if cmd_output == '':
	# no such font map in the system
	print 'no such font map in the TeX system.  Exit.'
elif cmd_output.find('pdftex') == -1:
	# such map file exist.  Continue.
	# '/usr/local/texlive/2008/../texmf-local/fonts/map/pdftex/cjk/bkaiu.map'
	# does the path contains 'pdftex'?
	# No.
	print 'this font map file is not associated with pdftex.  Exit.'
else:
	# this map exist.  It is associated with pdftex.
	# We have the ticket to continue.

	for fn_indx in range(len(fixfn)):
		# process dvipdfm_dl14.map and dvipdfm_ndl14.map one by one
		
		# locate the active dvipdfm.map
		cmd_fid=os.popen(texbin + kp + ' ' + fixfn[fn_indx])
		fixpath=cmd_fid.readline()
		
		#remove the trailing '\n'
		fixpath=fixpath.replace('\n','')
		
		#open a temporary write file
		output_file_name='./' + fixfn[fn_indx] + '_' + font_name + '.temp'
		out_fid=open(output_file_name,'w')
		
		# the counter to count the fix ocurrence
		fix_counter=0
		
		# read the dvipdfm.map file line by line
		for line in fileinput.input(fixpath):
			# strip the possible '\r' ending (DOS line ending)
			line=line.replace('\r','')
			# strip the end of line '\n'
			line=line.replace('\n','')
			
			if line.find(font_name) != -1:
				#the line may need processing
				#something similar to the following
				# bkaiu00 bkaiu00 <bkai00mp.ttf
				
				# split them into three
				seg=line.split()
				
				# check the third to determine if the line needs fixing
				if (len(seg) > 2) and (seg[2].find('<') != -1):
					# yes, it needs fixing
					out_fid.write(seg[0] + ' ' + seg[1] + '\n')
					fix_counter=fix_counter+1
				else:
					# no need to fix
					out_fid.write(line + '\n')
			else:
				# other lines
				out_fid.write(line + '\n')
		out_fid.close()
		fileinput.close()
	
		if fix_counter > 0:
			# at least one fix occurred
			# replace the old dvipdfm.map
			print 'need to replace ' + fixfn[fn_indx] + ': replacing...'
			cmd_fid=os.popen3('rm ' + fixpath)
			cmd_err=cmd_fid[2].readline()
			if  cmd_err != '':
				print cmd_err
				print 'the replacement fails.'
			else:
				cmd_fid=os.popen3('mv ' + output_file_name + ' ' + fixpath)
				cmd_err=cmd_fid[2].readline()
				if cmd_err != '':
					print cmd_err
					print 'the replacement fails.'	
		else:
			# the original file is good.
			print fixfn[fn_indx]+ ' is OK.  No replacement is needed.'
			cmd_fid=os.popen('rm ' + output_file_name)
		
	
	
