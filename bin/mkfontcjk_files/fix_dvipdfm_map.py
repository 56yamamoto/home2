#! /usr/bin/python
# For The TexLive 2008 issued on Feb. 2008
# Its updmap-sys has a bug, which prevents the command to generate
# a proper dvipdfm_dl14.map,
# with respect to Chinese TrueType font map (originally for pdftex)
# updmap-sys propagates the font map info into dvipdfm_dl14.map
# with incorrect syntax
#
# the entry of the incorrect syntax:
# bkaiu00 bkaiu00 <bkai00mp.ttf
# 
# this script will scan the active dvipdfm_dl14.map and dvipdfm_ndl14.map
#   and look for the specified Chinese TrueType font entry
#
# modify them into
# bkaiu00 bkaiu00
#
# usage syntax:
# fix_dvipdfm_map.py
# 
# written by
# Nien-Po Chen  <npchen@saturn.yzu.edu.tw>
# Apr. 17, 2008

# the following path may need modification in different platforms
# so that /usr/texbin/kpsewhich execute the command 'kpsewhich'
# texbin='/usr/texbin/'
texbin='/usr/local/texlive/2008/bin/x86_64-linux/'
# the command 'kpsewhich'
kp='kpsewhich'

# the name of the file to be fixed
fixfn=['dvipdfm_dl14.map','dvipdfm_ndl14.map']

import os, sys, string, fileinput


for fn_indx in range(len(fixfn)):
	# process dvipdfm_dl14.map and dvipdfm_ndl14.map one by one
	
	# locate the active dvipdfm.map
	cmd_fid=os.popen(texbin + kp + ' ' + fixfn[fn_indx])
	fixpath=cmd_fid.readline()
	
	#remove the trailing '\n'
	fixpath=fixpath.replace('\n','')
	
	#open a temporary write file
	output_file_name='./' + fixfn[fn_indx] + '_'  + '.temp'
	out_fid=open(output_file_name,'w')
	
	# the counter to count the fix ocurrence
	fix_counter=0
	
	# read the dvipdfm.map file line by line
	for line in fileinput.input(fixpath):
		# strip the possible '\r' ending (DOS line ending)
		line=line.replace('\r','')
		# strip the end of line '\n'
		line=line.replace('\n','')
		
		if (line.find('<') != -1):
			#the line may need processing
			#something similar to the following
			# bkaiu00 bkaiu00 <bkai00mp.ttf
			
			# split them into fields
			seg=line.split()
			
			# check this line in details
			# to determine if the line needs fixing
			# 	more than 2 columns
			#	first two columns are identical
			#	third column contains '<'
			#	third column contains '.ttf' (case insensitive)
			if (len(seg) > 2) and (seg[0] == seg[1]) and (seg[2].find('<') != -1) and (seg[2].lower().find('.ttf') != -1):
				# yes, it needs fixing
				out_fid.write(seg[0] + ' ' + seg[1] + '\n')
				fix_counter=fix_counter+1
			else:
				# no need to fix
				out_fid.write(line + '\n')
		else:
			# other lines
			out_fid.write(line + '\n')
	out_fid.close()
	fileinput.close()

	if fix_counter > 0:
		# at least one fix occurred
		# replace the old dvipdfm.map
		print 'need to fix ' + fixfn[fn_indx] + ': fixing...'
		cmd_fid=os.popen3('rm ' + fixpath)
		cmd_err=cmd_fid[2].readline()
		if  cmd_err != '':
			print cmd_err
			print 'the deletion of the original file fails.'
		else:
			cmd_fid=os.popen3('mv ' + output_file_name + ' ' + fixpath)
			cmd_err=cmd_fid[2].readline()
			if cmd_err != '':
				print cmd_err
				print 'the replacement of the new file fails.'	
	else:
		# the original file is good.
		print fixfn[fn_indx]+ ' is OK.  No fix is needed.'
		cmd_fid=os.popen('rm ' + output_file_name)
		
	
