#!/bin/sh
cidxpath=`kpsewhich cid-x.map`
testpath="$HOME/texmf/fonts/map/dvipdfm/dvipdfmx/cid-x.map"

texmf_choice="/usr/local/texlive/texmf-local"
CIDXMAP='/fonts/map/dvipdfm/dvipdfmx'

sed_output=`echo $cidxpath |sed -n "/^\/usr/p" |sed -n "/texmf\/fonts/p"`
if ! [ "$sed_output" = '' ]
then
	# non-empty string
	echo "the path HAS keyword."
	cidxmap_location_bug=1
else
	# empty string
	echo "the path has NO keyword."
fi

if [ "$cidxmap_location_bug" = 1 ]
then
	echo "it has BUG."
else
	echo "it has NO bug"
fi

if ! [ -f ${texmf_choice}${CIDXMAP}/cid-x.map ] && [ "$cidxmap_location_bug" = 1 ]
then
	echo "texmf-local has NO cid-x.map and has bug"
else
	echo "texmf-local may have cid-x.map or NO bug"
fi

echo 'XX'`kpsewhich bkai.map`'XX'
