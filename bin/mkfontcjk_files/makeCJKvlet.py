#! /usr/bin/python
# create the 2nd half of the *.fdx file
# by reading the 1st half (from makefdx.pl pearl script)
# syntax:
# makeCJKvlet.py input.fdx output.fdx 
# written by
# Nien-Po Chen  <npchen@saturn.yzu.edu.tw>
# Sep. 5, 2006

import os, sys, string, fileinput
#print sys.argv[1]


##import glob
##import fileinput
##import string
##import os


input_file_name = sys.argv[1]
output_file_name = sys.argv[2]
##output_file_basename = os.path.split(input_file_name)[1]
##output_file_corename = output_file_basename.split('.')[0]
fid=open(output_file_name,'w')

for line in fileinput.input(input_file_name):
    # process line by line here
    # strip the possible '\r' ending (DOS line ending)
    line=line.replace('\r','')
    # strip the end of line '\n'
    line=line.replace('\n','')
    if line.find('CJKvdef') != -1:
        # the line needs processing
        # something similar to the following
        # \CJKvdef{m/n/30/16}{\def\CJK@plane{v}\selectfont\CJKsymbol{76}}
        loc=line.find('}')

        # \CJKvdef{m/n/30/16}
        line=line[0:(loc+1)]
        
        # \CJKvlet{m/n/30/16}
        line=line.replace('CJKvdef','CJKvlet')

        loc=line.find('{')
        # {m/n/30/16}
        seg1 = line[loc:]

        # \CJKvlet
        line=line[:loc]

        # \CJKvlet{bx/n/30/16}{m/n/30/16}
        line=line + seg1.replace('{m/n/','{bx/n/') + seg1 + '\n'

        # write to the file
        fid.write(line)

fid.close()
fileinput.close()

        
    
    


