#!/bin/sh
CIDXMAP='/fonts/map/dvipdfm/dvipdfmx'
texmf_choice='/usr/local/texlive/texmf-local'
cidxpath=`kpsewhich cid-x.map`
if ! [ "$cidxpath" = '' ]
then
	# non-empty string; cid-x.map exists
	cidxmap_exist=1
else
	# emtpy string; cid-x.map does NOT exists
	# this should not happen in TeXLive 2007 distribution
	cidxmap_exist=0
fi

echo "cidxmap_exist="${cidxmap_exist}

sed_output=`echo $cidxpath |sed -n "/^\/usr/p" |sed -n "/texmf\/fonts/p"`
if ! [ "$sed_output" = '' ]
then
	# non-empty string; the path HAS keyword.
	# cid-x.map is in [texmf]=/usr/local/texlive/2007/texmf
	cidxmap_location_bug=1
else
	# empty string; the path has NO keyword.
	# cid-x.map is at other location:
	# Maybe it is at [texmf]=~/Library/texmf
	# the bug will not cause any trouble
	#
	# Maybe it is at [texmf]=/usr/local/texlive/texmf-local
	#   ---> this means bug is fixed!
	# or, it does not exists at all.  
	#   This will be taken care of in the copying sage.
	cidxmap_location_bug=0
fi

echo "cidxmap_location_bug="${cidxmap_location_bug}

if [ "$cidxmap_exist" = 1 ]
then
	# the system has cid-x.map
	if ! [ -f ${texmf_choice}${CIDXMAP}/cid-x.map ]
	then
		# but it is not at texmf_choice
		# it is possible in the following two cases
		# 1) we want it to be home-texmf this time
		# duplicate a copy from the system
		# 2) the first time of font installation, 
		# cid-x.map is at texmf
		# but texmf_choice is texmf-local
		echo "sudo cp ${cidxpath} ${texmf_choice}${CIDXMAP}/cid-x.map"
	fi
	# there is cid-x.map at texmf_choice now.
	# pre-existing, or newly copied
	# update it
    echo "This script will append the new result to cid-x.map."
    #
    echo "cp ${texmf_choice}${CIDXMAP}/cid-x.map ./temp_cid_x.map"
    echo "cat ./cid-x.map >> ./temp_cid_x.map"
    echo "sudo rm ${texmf_choice}${CIDXMAP}/cid-x.map"
   	echo "sudo mv ./temp_cid_x.map ${texmf_choice}${CIDXMAP}/cid-x.map"
    echo "rm ./cid-x.map"
	#
	# if texmf_choice is texmf-local
	# and cidxmap_location_bug=1
	# we need to maintain another copy at texmf
	if [ "$cidxmap_location_bug" = 1 ] && [ "$texmf_choice" = "/usr/local/texlive/texmf-local" ]
	then
		# remove the old one at texmf
		echo "sudo rm ${cidxpath}"
		# copy the new one at texmf-local to texmf
		echo "sudo cp ${texmf_choice}${CIDXMAP}/cid-x.map ${cidxpath}"
	fi
else
	# the system has no cid-x.map at all
	# this should not happen in TeXLive2007 distribution
	# That's OK.  Create one from scratch.
	# move the newly created cid-x.map to the proper place
	echo "sudo mv ./cid-x.map ${texmf_choice}${CIDXMAP}/cid-x.map"
fi
