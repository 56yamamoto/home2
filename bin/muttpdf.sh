#!/bin/sh
tf=`mktemp`
# copy mutt's tempfile so it's not removed too early
cp "$1" "$tf"
pdftohtml "$tf"
www-browser "${tf#.pdf}".html
