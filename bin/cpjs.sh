#/bin/sh

cp $HOME/jsource/jgplsrc/release/Win32/jconsole.exe $JOPTHOME/j701/bin32/.
cp $HOME/jsource/jgplsrc/release/Win32/j.dll $JOPTHOME/j701/bin32/.
cp $HOME/jsource/jgplsrc/release/Win64/jconsole.exe $JOPTHOME/j701/bin/.
cp $HOME/jsource/jgplsrc/release/Win64/j.dll $JOPTHOME/j701/bin/.

cp $HOME/jsource/jgplsrc/release/Linux32/jconsole $JOPTHOME/j701/bin32/.
cp $HOME/jsource/jgplsrc/release/Linux32/libj.so $JOPTHOME/j701/bin32/.
cp $HOME/jsource/jgplsrc/release/Linux64/jconsole $JOPTHOME/j701/bin/.
cp $HOME/jsource/jgplsrc/release/Linux64/libj.so $JOPTHOME/j701/bin/.

cp $HOME/jsource/jgplsrc/release/Win32/jconsole.exe $JOPTHOME/j602/bin32/.
cp $HOME/jsource/jgplsrc/release/Win32/j.dll $JOPTHOME/j602/bin32/.
cp $HOME/jsource/jgplsrc/release/Win64/jconsole.exe $JOPTHOME/j602/bin/.
cp $HOME/jsource/jgplsrc/release/Win64/j.dll $JOPTHOME/j602/bin/.

cp $HOME/jsource/jgplsrc/release/Linux32/jconsole $JOPTHOME/j602/bin32/.
cp $HOME/jsource/jgplsrc/release/Linux32/libj.so $JOPTHOME/j602/bin32/.
cp $HOME/jsource/jgplsrc/release/Linux64/jconsole $JOPTHOME/j602/bin/.
cp $HOME/jsource/jgplsrc/release/Linux64/libj.so $JOPTHOME/j602/bin/.
