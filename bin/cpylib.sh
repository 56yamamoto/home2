#!/bin/sh 
# copy binaries from jbld to usr

A=$HOME/dev/jsource/jlibrary/bin
B=$HOME/dev/jsource/jlibrary/bin32

if [ "`uname`" = "Darwin" ]; then

if [ -d /opt/homebrew/lib ]; then
C=/opt/homebrew/lib
E=/opt/homebrew/bin
elif [ -d /usr/local/lib ]; then
C=/usr/loca/lib
E=/usr/loca/bin
else
exit 1
fi

if [ -f "$A/jconsole-mac" ] ; then
rm -f $E/ijconsole-9.03
cp "$A/jconsole-mac" $E/ijconsole-9.03
fi

if [ "`uname -m`" = "arm64" ]; then
if [ -f "$A/libj.dylib" ] ; then
rm $C/libj.dylib.9.03
cp $A/libj.dylib $C/libj.dylib.9.03
fi
else
if [ -f "$A/libjavx2.dylib" ] ; then
rm $C/libj.dylib.9.03
cp $A/libjavx2.dylib $C/libj.dylib.9.03
elif [ -f "$A/libjavx.dylib" ] ; then
rm $C/libj.dylib.9.03
cp $A/libjavx.dylib $C/libj.dylib.9.03
elif [ -f "$A/libj-nonavx.dylib" ] ; then
rm $C/libj.dylib.9.03
cp "$A/libj-nonavx.dylib" "$C/libj.dylib.9.03"
elif [ -f "$A/libj.dylib" ] ; then
rm $C/libj.dylib.9.03
cp "$A/libj.dylib" "$C/libj.dylib.9.03"
fi
fi

else

# linux/raspberry

if [ -d /usr/lib/aarch64-linux-gnu ] ; then
C=/usr/lib/aarch64-linux-gnu
elif [ -d /usr/lib/x86_64-linux-gnu ]; then
C=/usr/lib/x86_64-linux-gnu
else
C=/usr/lib64
fi

if [ -d /usr/lib/arm-linux-gnueabihf ] ; then
D=/usr/lib/arm-linux-gnueabihf
elif [ -d /usr/lib/i386-linux-gnu ]; then
D=/usr/lib/i386-linux-gnu
else
D=/usr/lib
fi

E=/usr/bin

if [ -f "$A/jconsole-lx" ] ; then
sudo cp "$A/jconsole-lx" $E/ijconsole-9.03
fi
if [ -f "$B/jconsole" ] ; then
sudo cp "$B/jconsole" $E/ijconsole_x86-9.03
fi

if [ -f "$A/libjavx2.so" ] ; then
sudo cp $A/libjavx2.so $C/libj.so.9.03
elif [ -f "$A/libjavx.so" ] ; then
sudo cp $A/libjavx.so $C/libj.so.9.03
elif [ -f "$A/libj-nonavx.so" ] ; then
sudo cp "$A/libj-nonavx.so" "$C/libj.so.9.03"
elif [ -f "$A/libj.so" ] ; then
sudo cp $A/libj.so $C/libj.so.9.03
fi
if [ -f "$B/libj.so" ] ; then
sudo cp "$B/libj.so" $D/libj.so.9.03
fi

sudo ldconfig

fi
