#include <stdio.h>
#include <fcntl.h>
#include <linux/kd.h>  /* Keyboard IOCTLs */
#include <sys/ioctl.h> /* ioctl()         */

#define KEYBOARDDEVICE	"/dev/console"

int main(int argc, char *argv[]) {
int		keyboardDevice = 0;
ulong	ledVal = 0L;	
if (! geteuid()) {	/* We are running as EUID root - CONSOLE */
	if (-1 == (keyboardDevice = open(KEYBOARDDEVICE, O_RDONLY))) {
		exit(1);
	}
	if (ioctl(keyboardDevice, KDGETLED, &ledVal)) {
    if (keyboardDevice) close(keyboardDevice);
		exit(1);
	}
	/* ledVal |= LED_SCR; */
	ledVal &= ~LED_SCR;
	if (ioctl(keyboardDevice, KDSETLED, (char)ledVal)) {
    if (keyboardDevice) close(keyboardDevice);
		exit(1);
	}
  if (keyboardDevice) close(keyboardDevice);
} else {		/* EUID not root */
	if (ioctl(0, KDGETLED, &ledVal) ) {
		exit (1);
	}
	if (ioctl(0, KDGETLED, &ledVal)) {
		exit(1);
	}
	/* ledVal |= LED_SCR; */
	ledVal &= ~LED_SCR;
	if (ioctl(0, KDSETLED, (char)ledVal)) {
		exit(1);
	}
}
exit(0);
}
