#!/bin/sh

cp $HOME/dev/jnet/JNet/bin/x64/Release/jnet.exe $HOME/share/ins.d/jnet-exe/bin/x64/.
cp $HOME/dev/jnet/JNet/bin/Release/jnet.exe     $HOME/share/ins.d/jnet-exe/bin/anycpu/.
cp $HOME/dev/jnet/JNet/bin/x86/Release/jnet.exe $HOME/share/ins.d/jnet-exe/bin/x86/.

cp $HOME/dev/jnet/JNet/bin/x64/Release/jnet.exe $HOME/jal/addons/ide/jnet/bin/x64/.
cp $HOME/dev/jnet/JNet/bin/Release/jnet.exe     $HOME/jal/addons/ide/jnet/bin/anycpu/.
cp $HOME/dev/jnet/JNet/bin/x86/Release/jnet.exe $HOME/jal/addons/ide/jnet/bin/x86/.
