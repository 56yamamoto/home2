#!/bin/sh 
# copy binaries from jbld to usr

A=$HOME/dev/jsource/jlibrary/bin
B=$HOME/dev/jsource/jlibrary/bin32

if [ -d /usr/lib/aarch64-linux-gnu ] ; then
C=/usr/lib/aarch64-linux-gnu
elif [ -d /usr/lib/x86_64-linux-gnu ]; then
C=/usr/lib/x86_64-linux-gnu
else
C=/usr/lib64
fi

if [ -d /usr/lib/arm-linux-gnueabihf ] ; then
D=/usr/lib/arm-linux-gnueabihf
elif [ -d /usr/lib/i386-linux-gnu ]; then
D=/usr/lib/i386-linux-gnu
else
D=/usr/lib
fi

E=/usr/bin

if [ -f "$A/jconsole-lx" ] ; then
sudo cp "$A/jconsole-lx" $E/ijconsole-9.02
fi
if [ -f "$B/jconsole-lx" ] ; then
sudo cp "$B/jconsole-lx" $E/ijconsole_x86-9.02
fi

if [ -f "$A/libjavx2.so" ] ; then
sudo cp $A/libjavx2.so $C/libj.so.9.02
elif [ -f "$A/libjavx.so" ] ; then
sudo cp $A/libjavx.so $C/libj.so.9.02
elif [ -f "$A/libj.so" ] ; then
sudo cp $A/libj.so $C/libj.so.9.02
fi
if [ -f "$A/libj-nonavx.so" ] ; then
sudo cp "$A/libj-nonavx.so" "$C/libj-nonavx.so.9.02"
fi
if [ -f "$B/libj.so" ] ; then
sudo cp "$B/libj.so" $D/libj.so.9.02
fi

sudo ldconfig
