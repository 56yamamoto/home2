#!/bin/sh
# gitcommitone "commit comment" file1 file2 ...
comment="$1"
shift
for name in "$@"
do
  f=`basename "$name"`
  git commit -m "$comment $f" -- "$name"
done
 
