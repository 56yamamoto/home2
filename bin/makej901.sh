#!/bin/sh

echo "*** remember to set jversion.h to j901 release-e first ***"

cd $HOME/dev/jsource/make2 && \
./clean.sh && \
jplatform=linux j64x=j64 ./build_libj.sh && \
./clean.sh && \
jplatform=linux j64x=j64avx ./build_libj.sh && \
./clean.sh && \
jplatform=linux j64x=j64avx2 ./build_jconsole.sh && \
jplatform=linux j64x=j64avx2 ./build_libj.sh && \
jplatform=linux j64x=j64avx2 ./build_tsdll.sh && \
jplatform=linux j64x=j64avx2 ./build_jnative.sh && \
jplatform=linux j64x=j32 ./build_jconsole.sh && \
jplatform=linux j64x=j32 ./build_libj.sh && \
jplatform=linux j64x=j32 ./build_tsdll.sh && \
jplatform=linux j64x=j32 ./build_jnative.sh && \
./cpbin.sh && \
 \
cd $HOME/dev/jsource/makemsvc/tsdll && \
ms19 make clean && ms19 make && \
ms1932 make clean && ms1932 make && \
 \
cd $HOME/dev/jsource/makemsvc/jconsole && \
ms19 make clean && ms19 make && \
ms1932 make clean && ms1932 make && \
 \
cd $HOME/dev/jsource/makemsvc/jdll && \
ms1932 make clean && ms1932 make && \
ms19 make clean && ms19 make JAVX2=1 && \
ms19 make clean && ms19 make JAVX=1 && \
ms19 make clean && ms19 make && \
 \
cpyj.sh && cpybeta901.sh \

# cpylib901.sh
echo cpylib901.sh
