#/bin/sh

find $HOME/project/jproject -type d -print0 |xargs -0 chmod g+rwx
find $HOME/project/jproject -type f -print0 |xargs -0 chmod g+rw

find $HOME/share/willis -type d -print0 |xargs -0 chmod g+rwx
find $HOME/share/willis -type f -print0 |xargs -0 chmod g+rw

find $HOME/share/ins.d -type d -print0 |xargs -0 chmod g+rwx
find $HOME/share/ins.d -type f -print0 |xargs -0 chmod g+rw
