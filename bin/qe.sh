#!/bin/sh
QEMU_AUDIO_DRV=none exec /usr/bin/qemu-system-arm \
  -machine versatilepb -cpu arm1176 -nographic -m 256 \
  -M versatilepb \
  -drive format=raw,file=$HOME/kvm/2019-04-08-raspbian-stretch-lite.img,cache=unsafe \
  -net nic -net user \
  -netdev tap,id=mmnet,ifname=tap3,script=no \
  -kernel $HOME/kvm/kernel-qemu-4.14.79-stretch \
  -append 'root=/dev/sda2 panic=1' \
  -no-reboot

#  -dtb /.../versatile-pb.dtb \
