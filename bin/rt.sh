#!/bin/sh

TMPFILE=`mktemp -t rttmp.XXXXXXXXX`

wget "$1" -O $TMPFILE -q -U "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)"
sed -i -e "/vpVideoSource/!d" $TMPFILE
sed -i -e 's/^.*= /mplayer -utf8 -fs -really-quiet -volume 5 /' $TMPFILE
sed -i -e 's/^.*= /mplayer /' $TMPFILE
sed -i -e 's/;$//' $TMPFILE
sed -i -e 's/\\//g' $TMPFILE
#exec sh $TMPFILE && rm $TMPFILE
CMD=`cat $TMPFILE`
rm $TMPFILE
eval $CMD
